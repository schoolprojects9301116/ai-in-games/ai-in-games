# AI-in-Games

# Controlling the Simulation
The user has no control over the actual simulation and instead will simply watch the ai agents play together

# Code breakup
Source code is broken up as follows:

## ActionManager
The AiActionManager class can be found in the include/AI/DecisionMaking/AiActionManager.h file with the .cpp file being located in the same location under src/ instead of include/.

## AiActions
The AiAction base class used by the AiActionManager can be found in include/AI/DecisionMaking/AiAction.h file with the .cpp file being located in the same location under src/ instead of include/.

Custom Actions are found in several locations:  
    1. include/HW3/CustomBehaviorTreeActions.h  
    2. include/HW3/DecisionTreeCustomNodes.h  
    3. include/AI/DecisionMaking/DecisionTrees/CustomDecisionTreeActions.h  

These implementains are what actually run and operate the AiAgents

## DecisionTrees
DecisionTrees are made of several node types. The core node types are found under include/AI/DecisionMaking/DecisionTrees/ and the custom nodes are found under include/HW3/DecisionTreeCustomNodes.h. These nodes set the foundation for how the tree can operate but are actually constructed under the AiControllerHW3 class.

## BehaviorTrees
BehaviorTrees are also made of several node types. The core node types are found under include/AI/DecisionMaking/BehaviorTrees/. The current implementation of these trees uses only base nodes and then passes specific actions to construct the tree. See AiControllerHW3 for more info

## Goap
All Goap related behavior is currently found under include/AI/DecisionMaking/Goap/

## AiController
The AiControllers for both player (blue) an monster (red) are found in include/HW3/AiControllerHW3.h in this file there are two sublcasses of AiControllerHW3; one for the player and the other for the monster. The constructor of each of these subclasses construct their respective decision making structures and then execute their decisions in the update functions.

# Writeup
Writeup is located in the Solution directory under the Writeup folder

