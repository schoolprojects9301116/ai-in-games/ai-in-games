#include "HW3/AiControllerHW3.h"
#include "AI/DecisionMaking/DecisionTrees/DecisionTreeNode.h"
#include "HW3/DecisionTreeCustomNodes.h"
#include "AI/DecisionMaking/BehaviorTrees/DecoratorNodes.h"
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTreeActionNode.h"
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTreeSelectorNode.h"
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTreeSequenceNode.h"
#include "HW3/CustomBehaviorTreeActions.h"

#include "AI/DecisionMaking/DecisionTrees/CustomDecisionTreeActions.h"


#include "ofGraphics.h"
#include "Engine/Core/GameObject.h"
#include <iostream>


AiControllerHW3::AiControllerHW3(game_engine::GameObject* gameObject
	, AiAgentComponent* agent, game_engine::maps::LevelMap* map)
		: game_engine::Component(gameObject), agent_(agent), map_(map)
{
}

AiControllerHW3::~AiControllerHW3() {}

void AiControllerHW3::Update(float dt)
{
	// DO THE THING!
}


void DrawAi(game_engine::GameObject* gameObject)
{
	AiPlayerController* aiPC = gameObject->GetCompenent<AiPlayerController>();
	if (aiPC != nullptr)
	{
		aiPC->DrawTarget();
	}
	DrawBoid(gameObject);
}

AiPlayerController::AiPlayerController(game_engine::GameObject* gameObject, AiAgentComponent* agent
		, game_engine::maps::LevelMap* map, AiActionManager& manager)
		: AiControllerHW3(gameObject, agent, map), decision_tree_(nullptr),
	target_position_({ nullptr }), current_action_(nullptr), manager_(manager)
{
	// TODO: Replace min max bounds
	decision_tree_ = GenerateCustomTree(gameObject, target_position_, *agent, target_path_, *map_, 1024, 768);
	gameObject->SetDrawFunction(&DrawAi);
}

AiPlayerController::~AiPlayerController()
{
	delete decision_tree_;
}

void AiPlayerController::Update(float dt)
{
	if (current_action_ == nullptr || current_action_->IsComplete())
	{
		current_action_ = &decision_tree_->MakeDecision();
		manager_.ScheduleAction(current_action_);
	}
}

void AiPlayerController::DrawTarget()
{
	if (target_position_.target != nullptr)
	{
		ofSetColor(ofColor::green);
		ofDrawCircle(*(target_position_.target), 5);
	}
}



void DrawMonsterAi(game_engine::GameObject* gameObject)
{
	DrawBoid(gameObject);
	AiMonsterController* aiMC = gameObject->GetCompenent<AiMonsterController>();
	if (aiMC != nullptr)
	{
		aiMC->DrawTarget();
	}
}

AiMonsterController::AiMonsterController(game_engine::GameObject* gameObject, AiAgentComponent* agent
			, AiAgentComponent* player, game_engine::maps::LevelMap* map, AiActionManager& manager) 
		: AiControllerHW3(gameObject, agent, map), target_position_({new ofVec2f(player->Position())})
	, target_timer_(gameObject, target_position_) , behavior_tree_(nullptr), manager_(manager)
{
	gameObject->SetDrawFunction(&DrawMonsterAi);
	gameObject->AddComponent(&target_timer_);

// construct bt
	auto rootNode = new BehaviorTreeSelectorNode("root");
	behavior_tree_ = new BehaviorTree(rootNode);

	auto nodeA = new BehaviorTreeSequenceNode("A");
	auto nodeB = new BehaviorTreeSelectorNode("B");
	auto nodeC = new BehaviorTreeSequenceNode("C");
	auto nodeD = new BehaviorTreeUntilSuccessNode("D");
	auto nodeE = new BehaviorTreeInverterNode("E");
	auto nodeF = new BehaviorTreeSequenceNode("F");

	auto actionA = new TimerAction(10, 0.5, 0.5, target_timer_);
	auto actionB = new GetTargetLocationAction(10, 0.5, player, target_position_, target_path_);
	auto actionC = new PathfindAction(10, 0.5, *agent_, target_position_, *map_, target_path_);
	auto actionD = new HasPathCondition(7, 2, target_path_);
	auto actionE = new ArriveOnPathAction(4, 1, *agent_, target_path_);
	auto actionF = new ProximityCheck(6, 0.5, 40, *agent_, target_path_);
	auto actionG = new ReducePathAction(7, 0.5, target_path_);

	auto actionNodeA = new BehaviorTreeConditionNode("actionA", actionA);
	auto actionNodeB = new BehaviorTreeActionNode("actionB", actionB);
	auto actionNodeC = new BehaviorTreeActionNode("actionC", actionC);
	auto actionNodeD = new BehaviorTreeConditionNode("actionD", actionD);
	auto actionNodeE = new BehaviorTreeActionNode("actionE", actionE);
	auto actionNodeF = new BehaviorTreeConditionNode("actionF", actionF);
	auto actionNodeG = new BehaviorTreeActionNode("actionG", actionG);

	rootNode->AddChild(nodeA);
	rootNode->AddChild(nodeB);

	nodeA->AddChild(actionNodeA);
	nodeA->AddChild(actionNodeB);

	nodeB->AddChild(nodeC);
	nodeB->AddChild(nodeD);

	nodeC->AddChild(nodeE);
	nodeC->AddChild(actionNodeC);

	nodeD->AddChild(nodeF);

	nodeE->AddChild(actionNodeD);

	nodeF->AddChild(actionNodeE);
	nodeF->AddChild(actionNodeF);
	nodeF->AddChild(actionNodeG);

}

AiMonsterController::~AiMonsterController()
{
	delete behavior_tree_;
	delete target_position_.target;
}

void AiMonsterController::Update(float dt)
{
	AiAction* action = behavior_tree_->Execute();
	if (action != nullptr)
	{
		//std::cout << action->Name() << std::endl;
		manager_.ScheduleAction(action);
	}
}

void AiMonsterController::DrawTarget()
{
	if (target_position_.target != nullptr)
	{
		ofSetColor(ofColor::purple);
		ofDrawCircle(*(target_position_.target), 5);
	}
	
	if (target_path_.size() > 0)
	{
		ofSetColor(ofColor::navy);
		ofDrawCircle(target_path_.top(), 5);
	}
}