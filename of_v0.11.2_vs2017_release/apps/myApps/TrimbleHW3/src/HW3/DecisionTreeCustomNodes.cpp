#include "HW3/DecisionTreeCustomNodes.h"
#include "AI/DecisionMaking/DecisionTrees/CustomDecisionTreeActions.h"
#include "Engine/Core/GameObject.h"
#include <random>


DecisionTreeNode* GenerateCustomTree(game_engine::GameObject* gameObject, TargetPosition& targetPosition
	, AiAgentComponent& agent, std::stack<ofVec2f>& targetPath, game_engine::maps::LevelMap& map, float maxX, float maxY)
{
	DecisionTreeBranchNode* rootNode = new DecisionTreeBranchNode();
	rootNode->SetCondition(new ValidTargetCondition(targetPosition));
	DecisionTreeActionNode* rootFalseNode = new DecisionTreeActionNode();
	rootFalseNode->SetAction(new RandomTargetAction(10, 10.0f, maxX, maxY, targetPosition));
	rootNode->SetFalseNode(rootFalseNode);
	DecisionTreeBranchNode* rootTrueNode = new DecisionTreeBranchNode();
	rootNode->SetTrueNode(rootTrueNode);

	TargetTimingComponent* ttc = new TargetTimingComponent(gameObject, targetPosition);
	TargetTimingCondition* minPursueTimer = new TargetTimingCondition(ttc, 1.0f);
	gameObject->AddComponent(ttc);
	rootTrueNode->SetCondition(minPursueTimer);
	DecisionTreeBranchNode* nodeA = new DecisionTreeBranchNode();
	DecisionTreeBranchNode* nodeB = new DecisionTreeBranchNode();
	rootTrueNode->SetFalseNode(nodeA);
	rootTrueNode->SetTrueNode(nodeB);

	nodeA->SetCondition(new RemainingStepsCondition(targetPath, 0));
	nodeB->SetCondition(new RemainingStepsCondition(targetPath, 1));// (agent, targetPosition, 25));

	DecisionTreeActionNode* falseActionA = new DecisionTreeActionNode();
	falseActionA->SetAction(new FollowPathAction(7, 1, 20, agent, targetPath));
	nodeA->SetFalseNode(falseActionA);
	DecisionTreeActionNode* trueActionA = new DecisionTreeActionNode();
	trueActionA->SetAction(new PathfindAction(10, 10, agent, targetPosition, map, targetPath));
	nodeA->SetTrueNode(trueActionA);

	DecisionTreeActionNode* falseActionB = new DecisionTreeActionNode();
	falseActionB->SetAction(new FollowPathAction(7, 1, 15, agent, targetPath));
	nodeB->SetFalseNode(falseActionB);
	DecisionTreeActionNode* trueActionB = new DecisionTreeActionNode();
	trueActionB->SetAction(new ArriveAction(5, 1, agent, targetPosition));
	nodeB->SetTrueNode(trueActionB);
	
	return rootNode;
}

// =============================================================================================================
// ==																										  ==
// ==										 Valid Target Condition											  ==
// ==																										  ==
// =============================================================================================================

ValidTargetCondition::ValidTargetCondition(TargetPosition& targetPosition) : target_position_(targetPosition)
{}

ValidTargetCondition::~ValidTargetCondition()
{}

bool ValidTargetCondition::CheckCondition()
{
	return (target_position_.target != nullptr);
}

// =============================================================================================================
// ==																										  ==
// ==										 Proximity Condition											  ==
// ==																										  ==
// =============================================================================================================

ProximityCondition::ProximityCondition(AiAgentComponent& agent, TargetPosition& targetPosition, float proximityThreshold)
	: agent_(agent), target_position_(targetPosition), proximity_threshold_(proximityThreshold)
{}

ProximityCondition::~ProximityCondition()
{}

bool ProximityCondition::CheckCondition()
{
	return (target_position_.target->distance(agent_.Position()) <= proximity_threshold_);
}

// =============================================================================================================
// ==																										  ==
// ==									 Remaining Steps Condition											  ==
// ==																										  ==
// =============================================================================================================

RemainingStepsCondition::RemainingStepsCondition(std::stack<ofVec2f>& pathStack, int stepThreshold)
	: path_stack_(pathStack), step_threshold_(stepThreshold)
{}

RemainingStepsCondition::~RemainingStepsCondition()
{}

bool RemainingStepsCondition::CheckCondition()
{
	return(path_stack_.size() <= step_threshold_);
}

// =============================================================================================================
// ==																										  ==
// ==										Target Timing Condition											  ==
// ==																										  ==
// =============================================================================================================

TargetTimingComponent::TargetTimingComponent(game_engine::GameObject* gameObject, TargetPosition& targetPosition)
		: game_engine::Component(gameObject), target_position_(targetPosition)
	, last_target_(FLT_MAX, FLT_MAX), elapsed_time_(0.0f)
{}

TargetTimingComponent::~TargetTimingComponent()
{}

void TargetTimingComponent::Update(float dt)
{
	if(target_position_.target != nullptr)
	{
		if (last_target_ != *(target_position_.target))
		{
			last_target_ = *(target_position_.target);
			elapsed_time_ = 0.0f;
		}
		elapsed_time_ += dt;
	}
}

float TargetTimingComponent::ElapsedTimeOnCurrentTarget()
{
	return elapsed_time_;
}

TargetTimingCondition::TargetTimingCondition(TargetTimingComponent* timingComponent, float timingThreshold)
	: timing_component_(timingComponent), timing_threshold_(timingThreshold)
{}

TargetTimingCondition::~TargetTimingCondition()
{}

bool TargetTimingCondition::CheckCondition()
{
	return (timing_component_->ElapsedTimeOnCurrentTarget() >= timing_threshold_);
}

// =============================================================================================================
// ==																										  ==
// ==								 Velocity Threshold Condition											  ==
// ==																										  ==
// =============================================================================================================

SpeedThresholdCondition::SpeedThresholdCondition(AiAgentComponent* agent, float speedThreshold)
	: agent_(agent), speed_threshold_(speedThreshold)
{}

SpeedThresholdCondition::~SpeedThresholdCondition()
{}

bool SpeedThresholdCondition::CheckCondition()
{
	return (agent_->Velocity().length() >= speed_threshold_);
}

// =============================================================================================================
// ==																										  ==
// ==											Random Condition											  ==
// ==																										  ==
// =============================================================================================================

RandomCondition::RandomCondition(float probability)
	: probablity_(probability)
{}

RandomCondition::~RandomCondition()
{}

bool RandomCondition::CheckCondition()
{
	float randomValue = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
	return(randomValue <= probablity_);
}
