#include "ofApp.h"
#include "Engine/Core/GameObject.h"
#include "AI/Core/AiAgentComponent.h"
#include "AI/Core/AiAgentManagerComponent.h"
#include "AI/Core/SteeringOutput.h"
#include "ofUtils.h"
#include "Hw1/AiStateTracker.h"
#include "AI/Core/BreadcrumbRenderer.h"
#include "Engine/Maps/MapGenerator.h"
#include "HW3/AiControllerHW3.h"

//--------------------------------------------------------------
void ofApp::setup(){
	// CONSTRUCT PLAYER
	game_engine::GameObject* aiAgentPlayerObject = new game_engine::GameObject();
	AiAgentComponent* aiPlayerAgent = new AiAgentComponent(aiAgentPlayerObject, ofVec2f(150, 200)
		, 0, 10, 20, 100, 50, 100, 100, ofColor::blue);
	aiAgentPlayerObject->AddComponent(aiPlayerAgent);
	game_objects_.push_back(aiAgentPlayerObject);

	game_engine::GameObject* levelMapObject = new game_engine::GameObject();
	game_engine::maps::LevelMap* levelMap = game_engine::maps::LevelMap::CreateLevelMap(levelMapObject, "GraphData\\LevelMap.aimap"
		, ofColor::black, 40, 60);
	levelMapObject->AddComponent(levelMap);
	game_objects_.push_back(levelMapObject);

	AiActionManager* playerManager = new AiActionManager(aiAgentPlayerObject);
	aiAgentPlayerObject->AddComponent(playerManager);
	AiPlayerController* aiController = new AiPlayerController(aiAgentPlayerObject, aiPlayerAgent, levelMap, *playerManager);
	aiAgentPlayerObject->AddComponent(aiController);

	// CONSTRUCT MONSTER
	game_engine::GameObject* aiAgentMonsterObject = new game_engine::GameObject();
	AiAgentComponent* aiMonsterAgent = new AiAgentComponent(aiAgentMonsterObject, ofVec2f(150, 500)
		, 0, 10, 20, 100, 50, 100, 100, ofColor::red);
	aiAgentMonsterObject->AddComponent(aiMonsterAgent);
	game_objects_.push_back(aiAgentMonsterObject);

	AiActionManager* monsterManager = new AiActionManager(aiAgentMonsterObject);
	aiAgentMonsterObject->AddComponent(monsterManager);
	AiMonsterController* monsterController = new AiMonsterController(aiAgentMonsterObject, aiMonsterAgent, aiPlayerAgent
		, levelMap, *monsterManager);
	aiAgentMonsterObject->AddComponent(monsterController);
}

//--------------------------------------------------------------
void ofApp::update(){
	float elapsedTime = ofGetElapsedTimef();
	float dt = elapsedTime - previously_elapsed_time_;
	previously_elapsed_time_ = elapsedTime;

	if (ofGetKeyPressed(98))
	{
		draw_breadcrumbs_ = true;
	}
	else if (ofGetKeyPressed(110))
	{
		draw_breadcrumbs_ = false;
	}

	for (auto& gameObject : game_objects_)
	{
		if(gameObject->Active())
		{
			gameObject->UpdateAllComponents(dt);
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	if (draw_breadcrumbs_)
	{
		Engine::BreadcrumbRenderer::Instance().Render();
	}
	for (auto& gameObject : game_objects_)
	{
		if (gameObject->Active())
		{
			gameObject->Draw();
		}
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	Hw1::AiStateTracker::Instance()->SetKeyStatus(key, true);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	Hw1::AiStateTracker::Instance()->SetKeyStatus(key, false);
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	if (button == 0)
	{
		Hw1::AiStateTracker::Instance()->SetMouseClickPosition(x, y);
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
