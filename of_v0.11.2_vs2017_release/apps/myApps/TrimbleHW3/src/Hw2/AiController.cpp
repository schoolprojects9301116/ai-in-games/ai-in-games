#include "Hw1/AiStateTracker.h"
#include "HW2/AiController.h"
#include "ofVec2f.h"
#include "AI/Core/MovementAlgorithms.h"
#include "AI/Core/RigidBody2D.h"

AiController::AiController(game_engine::GameObject* gameObject, AiAgentComponent* agent, game_engine::maps::LevelMap* map)
	: game_engine::Component(gameObject), agent_(agent), map_(map), target_position_(agent_->Position()), follow_path_()
{}

AiController::~AiController() {}

void AiController::Update(float dt)
{
	if (!Hw1::AiStateTracker::Instance()->HasMouseBeenClickedEver())
	{
		return;
	}
	ofVec2f clickedPos = Hw1::AiStateTracker::Instance()->GetMouseClickPosition();
	if (last_mouse_position_ != clickedPos)
	{
		last_mouse_position_ = clickedPos;
		target_position_ = clickedPos;
		ofVec2f directionVector = target_position_ - agent_->Position();
		directionVector.normalize();
		while(map_->DetectCollision(target_position_, 15))
		{
			target_position_ -= directionVector * 15;
		}
		follow_path_ = map_->Pathfind(agent_->Position(), target_position_);
	}
	
	if(!follow_path_.empty() && (follow_path_.top() - agent_->Position()).length() < 15)
	{
		follow_path_.pop();
	}

	if (follow_path_.empty())
	{
		if ((target_position_ - agent_->Position()).length() > 5.0f)
		{
			auto steeringOutput = MovementAlgorithms::Dynamic::ArrivePm(agent_->RigidBody()
					, RigidBody2D({ target_position_, {0, 0}, 0, 0, 0 })
				, agent_->MaxSpeed(), agent_->MaxLinearAcceleration(), 10, 5, .5f);
			agent_->DynamicUpdate(dt, steeringOutput);
		}
	}
	else
	{
		auto steeringOutput = MovementAlgorithms::Dynamic::ArrivePm(agent_->RigidBody()
			, RigidBody2D({ follow_path_.top(), {0, 0}, 0, 0, 0 })
			, agent_->MaxSpeed(), agent_->MaxLinearAcceleration(), 10, 5, .5f);
		agent_->DynamicUpdate(dt, steeringOutput);
	}

	agent_->Orientation(RadiansToDegrees(atan2f(agent_->Velocity().y, agent_->Velocity().x)));
}