#include "AI/Core/BreadcrumbRenderer.h"
#include "../../ofApp.h"

namespace Engine
{
	BreadcrumbRenderer* BreadcrumbRenderer::instance_ = nullptr;

	BreadcrumbRenderer& BreadcrumbRenderer::Instance()
	{
		if (instance_ == nullptr)
		{
			instance_ = new BreadcrumbRenderer();
		}
		return *instance_;
	}

	BreadcrumbRenderer::~BreadcrumbRenderer() 
	{}

	void BreadcrumbRenderer::Render() 
	{
		ofSetColor(ofColor::white);
		for (auto& crumb : crumbLocations_)
		{
			ofDrawCircle(crumb, 5);
		}
	}
	void BreadcrumbRenderer::AddCrumb(ofVec2f crumb) 
	{
		crumbLocations_.push_back(crumb);
	}
	void BreadcrumbRenderer::RemoveCrumb(ofVec2f crumb) 
	{
		auto crumbIter = std::find(crumbLocations_.begin(), crumbLocations_.end(), crumb);
		if (crumbIter != crumbLocations_.end())
		{
			crumbLocations_.erase(crumbIter);
		}
	}

	BreadcrumbRenderer::BreadcrumbRenderer() {}
}