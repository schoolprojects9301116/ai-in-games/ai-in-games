#include "AI/Core/RigidBody2D.h"
#include <math.h>

constexpr float kToRadians = PI / 180;
constexpr float kToDegrees = 180 / PI;

float DegreesToRadians(float degrees)
{
	return degrees * kToRadians;
}

float RadiansToDegrees(float radians)
{
	return radians * kToDegrees;
}