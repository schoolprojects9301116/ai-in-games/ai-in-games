#include "AI/Core/AiAgentManagerComponent.h"
#include "AI/Core/AiAgentComponent.h"
#include "../src/ofApp.h"
#include "Hw1/AiStateTracker.h"
#include "AI/Core/MovementAlgorithms.h"
#include "AI/Core/AdvancedMovementAlgorithms.h"
#include "ofMath.h"
#include "Engine/Core/GameObject.h"

bool KinematicMotion(AiAgentComponent* agent, bool resetPosition);
void SeekSteeringBehaviors(AiAgentComponent* agent, bool useKinematicMovement, bool useArrive, bool shouldFlee
	, float dt);
void WanderSteeringBehaviors(AiAgentComponent* agent, bool shouldFace, float dt);
void FlockingBehavior(std::vector<AiAgentComponent*>& agents, float dt);


AiAgentManagerComponent::AiAgentManagerComponent(game_engine::GameObject* gameObject) :
	game_engine::Component(gameObject)
{}

AiAgentManagerComponent::~AiAgentManagerComponent() {}

void AiAgentManagerComponent::CheckForStateChange()
{
	// K
	if (ofGetKeyPressed(107))
	{
		use_kinematic_movement_ = true;
	}
	// D
	else if (ofGetKeyPressed(100))
	{
		use_kinematic_movement_ = false;
	}
	// S
	else if (ofGetKeyPressed(115))
	{
		use_arrive_ = false;
	}
	// A
	else if (ofGetKeyPressed(97))
	{
		use_arrive_ = true;
	}
	// F
	else if (ofGetKeyPressed(102))
	{
		should_flee_ = true;
	}
	// G
	else if (ofGetKeyPressed(103))
	{
		should_flee_ = false;
	}
	// Z
	else if (ofGetKeyPressed(123))
	{
		should_face_ = false;
	}
	// X
	else if (ofGetKeyPressed(120))
	{
		should_face_ = true;
	}
}

void AiAgentManagerComponent::Update(float dt)
{
	if (ai_agents_.size() == 0)
	{
		return;
	}

	CheckForStateChange();

	if (Hw1::AiStateTracker::Instance()->GetState() != Hw1::Hw1State::kFlock && should_hide_flock_)
	{
		should_hide_flock_ = false;
		SetFlockStatus(false);
	}

	switch (Hw1::AiStateTracker::Instance()->GetState())
	{
	case Hw1::Hw1State::kKinematicMotion:
		if (state_changed_)
		{
			ai_agents_[0]->ClearCrumbs();
		}
		state_changed_ = KinematicMotion(ai_agents_[0], state_changed_);
		break;
	case Hw1::Hw1State::kSeek:
		state_changed_ = true;
		SeekSteeringBehaviors(ai_agents_[0], use_kinematic_movement_, use_arrive_, should_flee_, dt);
		break;
	case Hw1::Hw1State::kWander:
		state_changed_ = true;
		WanderSteeringBehaviors(ai_agents_[0], should_face_, dt);
		break;
	case Hw1::Hw1State::kFlock:
		state_changed_ = true;
		SetFlockStatus(true);
		should_hide_flock_ = true;
		FlockingBehavior(ai_agents_, dt);
		break;
	default:
		break;
	}
}

void AiAgentManagerComponent::SetFlockStatus(bool isActive)
{
	for (size_t i = 1; i < ai_agents_.size(); i++)
	{
		if (!isActive)
		{
			ai_agents_[i]->ClearCrumbs();
		}
		ai_agents_[i]->GetGameObject()->Active(isActive);
	}
}

void AiAgentManagerComponent::AddAiAgent(AiAgentComponent* aiAgent)
{
	ai_agents_.push_back(aiAgent);
}


bool KinematicMotion(AiAgentComponent* agent, bool resetPosition)
{
	if (agent == nullptr)
	{
		return true;
	}

	if (resetPosition)
	{
		agent->Position({ 50, (float)ofGetWindowHeight() - 50 });
		agent->SetVelocity({ 0, -100 });
		agent->Orientation(270);
		agent->SetRotation(0);
	}

	ofVec2f agentVelocity = agent->Velocity();
	if (agentVelocity.x > 0)
	{
		if (agent->Position().x >= ofGetWindowWidth() - 50)
		{
			agent->SetVelocity(glm::vec2(0, 100));
			agent->Orientation(90);
		}
	}
	else if (agentVelocity.x < 0)
	{
		if (agent->Position().x <= 50)
		{
			agent->SetVelocity(glm::vec2(0, -100));
			agent->Orientation(270);
		}
	}
	else if (agentVelocity.y > 0)
	{
		if (agent->Position().y >= ofGetWindowHeight() - 50)
		{
			agent->SetVelocity(glm::vec2(-100, 0));
			agent->Orientation(180);
		}
	}
	else if (agentVelocity.y < 0)
	{
		if (agent->Position().y <= 50)
		{
			agent->SetVelocity(glm::vec2(100, 0));
			agent->Orientation(0);
		}
	}
	else
	{
		return true;
	}
	return false;
}

void SeekSteeringBehaviors(AiAgentComponent* agent, bool useKinematicMovement, bool useArrive, bool shouldFlee
	, float dt)
{
	if (agent == nullptr)
	{
		return;
	}

	ofVec2f positionToSeek = Hw1::AiStateTracker::Instance()->GetMouseClickPosition();

	RigidBody2D target = { positionToSeek, {0, 0}, 0, 0 };

	if (useKinematicMovement)
	{
		KinematicSteeringOutput output 
			= MovementAlgorithms::Kinematic::Seek(agent->RigidBody(), target, agent->MaxSpeed(), shouldFlee);

		agent->SetVelocity(output.linearVelocity);
		float newOrientation = atan2f(output.linearVelocity.y, output.linearVelocity.x);
		agent->Orientation(RadiansToDegrees(newOrientation));
	}
	else
	{
		DynamicSteeringOutput output;
		if (useArrive)
		{
			output = MovementAlgorithms::Dynamic::ArrivePm(agent->RigidBody(), target, agent->MaxSpeed()
				, agent->MaxLinearAcceleration(), 150, 10, 1);
		}
		else
		{
			output 
				= MovementAlgorithms::Dynamic::Seek(agent->RigidBody(), target, agent->MaxLinearAcceleration()
					, shouldFlee);
		}

		agent->DynamicUpdate(dt, output);
		output = 
			MovementAlgorithms::Dynamic::LookWhereYouAreGoing(agent->RigidBody(), agent->MaxLinearAcceleration()
				, .1, 1, 0.1);
		agent->DynamicUpdate(dt, output);
	}
}

void WanderSteeringBehaviors(AiAgentComponent* agent, bool shouldFace, float dt)
{
	DynamicSteeringOutput output 
		= MovementAlgorithms::Dynamic::Wander(agent->RigidBody(), 150, 1, agent->MaxLinearAcceleration());
	agent->DynamicUpdate(dt, output);
	if (shouldFace)
	{
		RigidBody2D faceTarget({ agent->Position() + agent->Velocity(), {0, 0}, 0, 0, 0 });
		output = MovementAlgorithms::Dynamic::Face(agent->RigidBody(), faceTarget, 20, 1, 5, 1);
	}
	else
	{
		output = MovementAlgorithms::Dynamic::LookWhereYouAreGoing(agent->RigidBody(), 20, 1, 5, 1);
	}
	agent->DynamicUpdate(dt, output);
}

void FlockingBehavior(std::vector<AiAgentComponent*>& agents, float dt)
{
	std::vector<RigidBody2D> agentBodies;
	std::vector<DynamicSteeringOutput> steeringOutputs(agents.size());

	for (AiAgentComponent* aiAgent : agents)
	{
		agentBodies.push_back(aiAgent->RigidBody());
	}
	steeringOutputs[0] 
		= MovementAlgorithms::Dynamic::Wander(agents[0]->RigidBody(), 150, 1, agents[0]->MaxLinearAcceleration());

	for (size_t i = 1; i < agentBodies.size(); i++)
	{
		steeringOutputs[i] 
			= MovementAlgorithms::Dynamic::Flock(agentBodies, 0, i, agents[i]->MaxSpeed()
				, agents[i]->MaxLinearAcceleration(), 1);
	}

	for (size_t i = 0; i < agents.size(); i++)
	{
		agents[i]->DynamicUpdate(dt, steeringOutputs[i]);
		agents[i]->DynamicUpdate(dt
			, MovementAlgorithms::Dynamic::LookWhereYouAreGoing(agents[i]->RigidBody(), 10, 1, 10, 1));
	}
}