#include "AI/Core/RigidBody2D.h"
#include "AI/Core/SteeringOutput.h"
#include "AI/Core/MovementAlgorithms.h"

namespace MovementAlgorithms
{
namespace Dynamic
{

	DynamicSteeringOutput LookWhereYouAreGoing(const RigidBody2D& character
		, float maxRotationAcceleration
		, float targetTheta
		, float slowTheta
		, float timeToTarget)
	{
		if (character.velocity.length() < 0.001)
		{
			return { {0, 0}, 0 };
		}
		else
		{
			RigidBody2D target({{ 0, 0 }, { 0, 0 }, 0, 0, 0});
			target.orientation = RadiansToDegrees(atan2f(character.velocity.y, character.velocity.x));
			return Align(character, target, maxRotationAcceleration, targetTheta, slowTheta, timeToTarget);
		}
	}

	DynamicSteeringOutput Face(const RigidBody2D& character, RigidBody2D target, float maxRotationAcceleration
		, float targetTheta
		, float slowTheta
	  , float timeToTarget)
	{
		ofVec2f direction = target.position - character.position;
		if (direction.length() < 0.01)
		{
			return { {0, 0}, 0 };
		}
		else
		{
			target.orientation = RadiansToDegrees(atan2f(direction.y, direction.x));
		}
		return Align(character, target, maxRotationAcceleration, targetTheta, slowTheta, timeToTarget);
	}

	DynamicSteeringOutput Seperation(std::vector<RigidBody2D>& agents, size_t activeAgentIndex, float maxAcceleration
		, float seperationRadius)
	{
		DynamicSteeringOutput output({ { 0, 0 }, 0 });
		for (size_t i = 0; i < agents.size(); i++)
		{
			if (i != activeAgentIndex)
			{
				output.linearAcceleration += 
					Seek(agents[activeAgentIndex], agents[i], maxAcceleration, true).linearAcceleration;
			}
		}
		if (output.linearAcceleration.length() > maxAcceleration)
		{
			output.linearAcceleration.normalize();
			output.linearAcceleration *= maxAcceleration;
		}
		return output;
	}

	DynamicSteeringOutput Flock(std::vector<RigidBody2D>& agents, size_t leaderIndex, size_t activeAgentIndex
		, float maxSpeed
		, float maxLinearAcceleration
	  , float timeToTarget)
	{
		DynamicSteeringOutput output = Seperation(agents, activeAgentIndex, maxLinearAcceleration, 20);
		output.linearAcceleration *= .8f;

		RigidBody2D flockStats({ {0,0}, {0, 0}, 0, 0, 0 });

		for (size_t i = 0; i < agents.size(); i++)
		{
			flockStats.position += agents[i].position * agents[i].mass;
			flockStats.velocity += agents[i].velocity * agents[i].mass;
			flockStats.mass += agents[i].mass;
		}

		flockStats.position /= flockStats.mass;
		flockStats.velocity /= flockStats.mass;
		output.linearAcceleration +=
			ArrivePm(agents[activeAgentIndex], flockStats, maxSpeed, maxLinearAcceleration
				, 75, 50, timeToTarget).linearAcceleration;

		output.linearAcceleration += 
			VelocityMatch(agents[activeAgentIndex], flockStats, maxLinearAcceleration, timeToTarget).linearAcceleration * .2f;

		return output;
	}

}
}