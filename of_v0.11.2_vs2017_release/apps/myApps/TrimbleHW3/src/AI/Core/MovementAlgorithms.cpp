#include "AI/Core/MovementAlgorithms.h"
#include "AI/Core/RigidBody2D.h"
#include "AI/Core/SteeringOutput.h"
#include "Engine/Math/Random.h"

namespace MovementAlgorithms
{
namespace Kinematic
{
	KinematicSteeringOutput Seek(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxSpeed
		, bool  shouldFlee)
	{
		ofVec2f seekVector;
		if (shouldFlee)
		{
			seekVector = character.position - target.position;
		}
		else
		{
			seekVector = target.position - character.position;
		}
		seekVector.normalize();
		seekVector *= maxSpeed;
		return { seekVector, 0 };
	}
}
namespace Dynamic
{
	DynamicSteeringOutput Seek(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxLinearAcceleration
		, bool  shouldFlee)
	{
		ofVec2f seekVector;
		if (shouldFlee)
		{
			seekVector = character.position - target.position;
		}
		else
		{
			seekVector = target.position - character.position;
		}
		seekVector.normalize();
		seekVector *= maxLinearAcceleration;
		return { seekVector, 0 };
	}

	DynamicSteeringOutput ArrivePm(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxSpeed
		, float maxLinearAcceleration
		, float slowRadius
		, float targetRadius
		, float timeToTarget)
	{
		ofVec2f differenceVector = target.position - character.position;
		ofVec2f targetDirection = differenceVector.getNormalized();
		float distance = differenceVector.length();
		float targetSpeed = 0;
		if (distance > slowRadius)
		{
			targetSpeed = maxSpeed;
		}
		else if (distance > targetRadius)
		{
			targetSpeed = maxSpeed * (distance - targetRadius) / (slowRadius - targetRadius);
		}
		ofVec2f targetVelocity = targetDirection * targetSpeed;
		ofVec2f velocityDifference = targetVelocity - character.velocity;
		ofVec2f linearAcceleration = velocityDifference / timeToTarget;
		if (linearAcceleration.getNormalized().length() > maxLinearAcceleration)
		{
			linearAcceleration.normalize() *= maxLinearAcceleration;
		}
		return { linearAcceleration, 0 };
	}

	DynamicSteeringOutput Align(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxRotationAcceeleration
		, float targetTheta
		, float slowTheta
		, float timeToTarget)
	{
		float diffTheta = target.orientation - character.orientation;
		while (diffTheta > 180)
		{
			diffTheta -= 360;
		}
		while (diffTheta < -180)
		{
			diffTheta += 360;
		}
		float diffThetaSize = abs(diffTheta);
		if (diffTheta < 0)
		{
			maxRotationAcceeleration = -maxRotationAcceeleration;
		}
			
		if (diffThetaSize < targetTheta)
		{
			return { {0, 0} , 0 };
		}
		else if (diffThetaSize > slowTheta)
		{
			return { { 0, 0 }, maxRotationAcceeleration };
		}
		else
		{
			float lerpFactor = (diffThetaSize - targetTheta) / (slowTheta - targetTheta);
			float rotationDiff = character.rotation - maxRotationAcceeleration * lerpFactor;
			rotationDiff /= timeToTarget;
			return { {0, 0}, rotationDiff };
		}
	}

	DynamicSteeringOutput Wander(const RigidBody2D& character, float offset, float maxRate
		, float maxLinearAcceleration)
	{
		float randomBinomial = Engine::RandomBinomial();
		RigidBody2D target({ { 0, 0 }, { 0, 0 }, 0, 0 });
		target.orientation = character.orientation + maxRate * randomBinomial;
		ofVec2f direction(cosf(target.orientation), sinf(target.orientation));
		direction *= offset;
		target.position = character.position + direction;
		return Seek(character, target, maxLinearAcceleration, false);
	}

	DynamicSteeringOutput VelocityMatch(const RigidBody2D& character, const RigidBody2D& target
		, float maxLinearAcceleration, float timeToTarget)
	{
		ofVec2f velocityDiff = target.velocity - character.velocity;
		ofVec2f linearAcceleration = velocityDiff / timeToTarget;
		if (linearAcceleration.length() > maxLinearAcceleration)
		{
			linearAcceleration.normalize();
			linearAcceleration *= maxLinearAcceleration;
		}

		return { linearAcceleration, 0 };
	}

}
}