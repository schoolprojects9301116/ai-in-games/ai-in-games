#include "AI/Core/AIAgentComponent.h"
#include "Engine/Core/GameObject.h"
#include "ofGraphics.h"
#include "../../ofApp.h"
#include "AI/Core/BreadcrumbRenderer.h"

void DrawBoid(game_engine::GameObject* gameObject)
{
	AiAgentComponent* agent = gameObject->GetCompenent<AiAgentComponent>();
	if (agent != nullptr)
	{
		agent->DrawAiAgent();
	}
}

// ========================================================================================================

AiAgentComponent::AiAgentComponent(game_engine::GameObject* gameObject
	, ofVec2f initialPosition
	, float initialOrientation
	, float circleRadius
	, float noseLength
	, float maxSpeed
	, float maxLinearAcceleration
	, float maxRotationSpeed
	, float mass
	, ofColor color) :
	game_engine::Component(gameObject)
	, rb2d_({ initialPosition, ofVec2f(0, 0), initialOrientation, 0.0f, mass })
	, boid_circle_radius(circleRadius)
	, boid_nose_scale_(noseLength)
	, max_speed_(maxSpeed)
	, max_linear_acceleration_(maxLinearAcceleration)
	, max_rotation_speed_(maxRotationSpeed)
	, color_(color)
{
	gameObject->SetDrawFunction(&DrawBoid);
}

AiAgentComponent::~AiAgentComponent() {}

void AiAgentComponent::Update(float dt) 
{
	if (bread_crumbs_.size() == 0)
	{
		bread_crumbs_.push_back(rb2d_.position);
		Engine::BreadcrumbRenderer::Instance().AddCrumb(rb2d_.position);

		bread_crumb_index_ = 1;
	}

	rb2d_.position += rb2d_.velocity * dt;
	if (rb2d_.position.x < -boid_nose_scale_)
	{
		rb2d_.position.x = ofGetWindowWidth() + boid_nose_scale_;
	}
	else if (rb2d_.position.x > ofGetWindowWidth() + boid_nose_scale_)
	{
		rb2d_.position.x = -boid_nose_scale_;
	}
	if (rb2d_.position.y < -boid_nose_scale_)
	{
		rb2d_.position.y = ofGetWindowHeight() + boid_nose_scale_;
	}
	else if (rb2d_.position.y > ofGetWindowHeight() + boid_nose_scale_)
	{
		rb2d_.position.y = -boid_nose_scale_;
	}

	Orientation(rb2d_.orientation + rb2d_.rotation * dt);

	size_t compareIndex = bread_crumb_index_ == 0 ? bread_crumbs_.size() - 1 : bread_crumb_index_ - 1;
	if ((rb2d_.position - bread_crumbs_[compareIndex]).length() > 30)
	{
		if (bread_crumbs_.size() < 10)
		{
			bread_crumbs_.push_back(rb2d_.position);
			Engine::BreadcrumbRenderer::Instance().AddCrumb(rb2d_.position);
		}
		else
		{
			Engine::BreadcrumbRenderer::Instance().RemoveCrumb(bread_crumbs_[bread_crumb_index_]);
			bread_crumbs_[bread_crumb_index_] = rb2d_.position;
			Engine::BreadcrumbRenderer::Instance().AddCrumb(rb2d_.position);
		}
		bread_crumb_index_++;
		if (bread_crumb_index_ == 10)
		{
			bread_crumb_index_ = 0;
		}
	}
}

void AiAgentComponent::DrawAiAgent()
{
	/*ofSetColor(ofColor::white);
	for (const auto& position : bread_crumbs_)
	{
		ofDrawCircle(position, 5);
	}*/
	ofSetColor(color_);
	ofPushMatrix();
	ofTranslate(rb2d_.position);
	ofRotateDeg(rb2d_.orientation);
	ofDrawCircle({ 0, 0 }, boid_circle_radius);
	ofDrawTriangle({ 0, boid_circle_radius }, { 0, -boid_circle_radius }, { boid_nose_scale_, 0 });
	ofPopMatrix();
}

void AiAgentComponent::DynamicUpdate(float dt, DynamicSteeringOutput steeringOutput)
{
	SetVelocity(rb2d_.velocity + steeringOutput.linearAcceleration * dt);
	SetRotation(rb2d_.rotation + steeringOutput.rotationalAcceleration * dt);
}

BoidTriangle AiAgentComponent::CalculateBoidTrianglePoints()
{
	ofVec2f thisPosition = Position();

	BoidTriangle boidTriangle({ thisPosition, thisPosition, thisPosition });

	float thisOrientation = DegreesToRadians(Orientation());
	float ninetyDegInRad = DegreesToRadians(90);

	boidTriangle.nosePos += (ofVec2f(cosf(thisOrientation), sinf(thisOrientation)) *= boid_nose_scale_);

	thisOrientation += ninetyDegInRad;
	boidTriangle.bodyPos1 += (ofVec2f(cosf(thisOrientation), sinf(thisOrientation)) *= boid_circle_radius);

	thisOrientation += (ninetyDegInRad * 2);
	boidTriangle.bodyPos2 += (ofVec2f(cosf(thisOrientation), sinf(thisOrientation)) *= boid_circle_radius);

	return boidTriangle;
}

void AiAgentComponent::SetVelocity(ofVec2f newVelocity)
{
	if (newVelocity.length() > max_speed_)
	{
		newVelocity.normalize() *= max_speed_;
	}
	rb2d_.velocity = newVelocity;
}

void AiAgentComponent::SetRotation(float newRotation)
{
	if (abs(newRotation) > max_rotation_speed_)
	{
		newRotation = newRotation < 0 ? -max_rotation_speed_ : max_rotation_speed_;
	}
	rb2d_.rotation = newRotation;
}

ofVec2f AiAgentComponent::Velocity() const
{
	return rb2d_.velocity;
}

ofVec2f AiAgentComponent::Position() const
{
	return rb2d_.position;
}

void AiAgentComponent::Position(ofVec2f newPosition)
{
	rb2d_.position = newPosition;
}

void AiAgentComponent::Orientation(float newOrientation)
{
	while (newOrientation > 360)
	{
		newOrientation -= 360;
	}
	while (newOrientation < 0)
	{
		newOrientation += 360;
	}
	rb2d_.orientation = newOrientation;
}

float AiAgentComponent::Orientation() const
{
	return rb2d_.orientation;
}

float AiAgentComponent::MaxSpeed()
{
	return max_speed_;
}

void AiAgentComponent::MaxSpeed(float newMaxSpeed) 
{
	max_speed_ = newMaxSpeed;
}

float AiAgentComponent::MaxLinearAcceleration() 
{
	return max_linear_acceleration_;
}

float AiAgentComponent::GetBoidNoseLength()
{
	return boid_nose_scale_;
}

void AiAgentComponent::MaxLinearAcceleration(float newMaxAcceleration) 
{
	max_linear_acceleration_ = newMaxAcceleration;
}

RigidBody2D AiAgentComponent::RigidBody() const
{
	return rb2d_;
}

void AiAgentComponent::ClearCrumbs()
{
	for (auto& crumb : bread_crumbs_)
	{
		Engine::BreadcrumbRenderer::Instance().RemoveCrumb(crumb);
	}
	bread_crumbs_.clear();
	bread_crumb_index_ = 0;
}