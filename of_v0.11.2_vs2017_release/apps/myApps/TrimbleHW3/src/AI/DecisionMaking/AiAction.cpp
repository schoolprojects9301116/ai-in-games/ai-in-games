#include "AI/DecisionMaking/AiAction.h"

AiAction::AiAction(int priority, float expiryTime, std::string name) : priority_(priority), queued_time_(0)
, expiry_time_(expiryTime), status_(AiActionStatus::ACTION_NONE), is_complete_(false), name_(name)
{}

int AiAction::Priority() const 
{ 
	return priority_; 
}

bool AiAction::Expired() const 
{
	return queued_time_ >= expiry_time_;
}

bool AiAction::IsComplete() const
{
	return is_complete_;
}

void AiAction::UpdateQueuedTime(float dt) 
{ 
	queued_time_ += dt;
}

void AiAction::TerminateAction()
{
	status_ = AiActionStatus::ACTION_FAILED;
	is_complete_ = true;
}

void AiAction::CompleteAction()
{
	status_ = AiActionStatus::ACTION_SUCCESS;
	is_complete_ = true;
}

void AiAction::ScheduleAction()
{
	status_ = AiActionStatus::ACTION_NONE;
	is_complete_ = false;
}

AiActionStatus AiAction::Status() const
{
	return status_;
}

std::string AiAction::Name() const
{
	return name_;
}

AiConditionAction::AiConditionAction(int priority, float expiryTime, std::string actionName) 
	: AiAction(priority, expiryTime, actionName), result_(false)
{}

AiConditionAction::~AiConditionAction()
{}

bool AiConditionAction::GetResult()
{
	return result_;
}