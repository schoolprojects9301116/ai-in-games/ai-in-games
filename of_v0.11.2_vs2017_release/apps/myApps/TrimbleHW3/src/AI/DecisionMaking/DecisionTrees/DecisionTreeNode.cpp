#include "AI/DecisionMaking/DecisionTrees/DecisionTreeNode.h"

DecisionTreeNode::DecisionTreeNode() {}
DecisionTreeNode::~DecisionTreeNode() {}

DecisionTreeActionNode::DecisionTreeActionNode() : DecisionTreeNode(), action_(nullptr)
{}

DecisionTreeActionNode::~DecisionTreeActionNode()
{
	if (action_ != nullptr)
	{
		delete action_;
		action_ = nullptr;
	}
}

void DecisionTreeActionNode::SetAction(AiAction* action)
{
	action_ = action;
}


AiAction& DecisionTreeActionNode::MakeDecision()
{
	return *action_;
}

DecisionTreeBranchCondition::DecisionTreeBranchCondition() {}

DecisionTreeBranchCondition::~DecisionTreeBranchCondition() {}

DecisionTreeBranchNode::DecisionTreeBranchNode() 
	: DecisionTreeNode(), true_node_(nullptr), false_node_(nullptr), condition_(nullptr)
{}

DecisionTreeBranchNode::~DecisionTreeBranchNode()
{
	if (true_node_ != nullptr)
	{
		delete true_node_;
		true_node_ = nullptr;
	}
	if (false_node_ != nullptr)
	{
		delete false_node_;
		false_node_ = nullptr;
	}
	if (condition_ != nullptr)
	{
		delete condition_;
		condition_ = nullptr;
	}
}

AiAction& DecisionTreeBranchNode::MakeDecision()
{
	if (condition_->CheckCondition())
	{
		return true_node_->MakeDecision();
	}
	else
	{
		return false_node_->MakeDecision();
	}
}

void DecisionTreeBranchNode::SetTrueNode(DecisionTreeNode* trueNode)
{
	true_node_ = trueNode;
}

void DecisionTreeBranchNode::SetFalseNode(DecisionTreeNode* falseNode)
{
	false_node_ = falseNode;
}

void DecisionTreeBranchNode::SetCondition(DecisionTreeBranchCondition* condition)
{
	condition_ = condition;
}
