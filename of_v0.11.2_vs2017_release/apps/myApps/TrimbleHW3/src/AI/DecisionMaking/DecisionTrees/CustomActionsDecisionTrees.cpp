#include "AI/DecisionMaking/DecisionTrees/CustomDecisionTreeActions.h"
#include "AI/Pathfinding/Graph.h"
#include "AI/Core/MovementAlgorithms.h"
#include <random>

RandomTargetAction::RandomTargetAction(int priority, float expiryTime, float maxX, float maxY, TargetPosition& targetPosition)
	: AiAction(priority, expiryTime, "RandomTargetAction"), max_x_(maxX), max_y_(maxY), target_position_(targetPosition)
{}

RandomTargetAction::~RandomTargetAction()
{}

bool RandomTargetAction::CanInterrupt() const
{
	return false;
}

bool RandomTargetAction::CanDoBoth(const AiAction& other) const
{
	return false;
}

void RandomTargetAction::Execute(float dt)
{
	float randX = static_cast<float> (rand()) / static_cast<float> (RAND_MAX);
	float randY = static_cast<float> (rand()) / static_cast<float> (RAND_MAX);
	randX *= max_x_;
	randY *= max_y_;

	if (target_position_.target == nullptr)
	{
		target_position_.target = new ofVec2f;
	}
	target_position_.target->set(randX, randY);
}

PathfindAction::PathfindAction(int priority, float expiryTime, AiAgentComponent& agent, TargetPosition& targetPosition
		, game_engine::maps::LevelMap& map, std::stack<ofVec2f>& pathStack)
		: AiAction(priority, expiryTime, "Pathfind Action"), agent_(agent), target_position_(targetPosition), map_(map
	), path_stack_(pathStack)
{}

PathfindAction::~PathfindAction()
{}

bool PathfindAction::CanInterrupt() const
{
	return false;
}

bool PathfindAction::CanDoBoth(const AiAction& other) const
{
	return false;
}

void PathfindAction::Execute(float dt)
{
	if(target_position_.target == nullptr)
	{
		target_position_.target = new ofVec2f(150, 200);
	}
	path_stack_ = map_.Pathfind(agent_.Position(), *(target_position_.target));
	if (path_stack_.size() == 0)
	{
		delete target_position_.target;
		target_position_.target = nullptr;
	}
}

FollowPathAction::FollowPathAction(int priority, float expiryTime, float pointThreshold, AiAgentComponent& agent
		, std::stack<ofVec2f>& path) : AiAction(priority, expiryTime, "Follow Path Action")
	, point_threshold_(pointThreshold), agent_(agent), path_(path)
{}

FollowPathAction::~FollowPathAction()
{}

bool FollowPathAction::CanInterrupt() const
{
	return true;
}

bool FollowPathAction::CanDoBoth(const AiAction& other) const
{
	return false;
}

void FollowPathAction::Execute(float dt)
{
	if (path_.size() > 0)
	{
		ofVec2f nextPoint = path_.top();
		while (path_.size() > 0 && agent_.Position().distance(nextPoint) <= point_threshold_)
		{
			path_.pop();
			if (path_.size() > 0)
			{
				nextPoint = path_.top();
			}
			else
			{
				return;
			}
		}
		/*auto output = MovementAlgorithms::Dynamic::Seek(agent_.RigidBody(), { nextPoint }
			, agent_.MaxLinearAcceleration(), false);*/
		auto output = MovementAlgorithms::Dynamic::ArrivePm(agent_.RigidBody(), { nextPoint },
			agent_.MaxSpeed(), agent_.MaxLinearAcceleration(), 10, 5, 0.5);
		agent_.DynamicUpdate(dt, output);
		agent_.Orientation(RadiansToDegrees(atan2f(agent_.Velocity().y, agent_.Velocity().x)));
	}
}

ArriveAction::ArriveAction(int priority, float expiryTime, AiAgentComponent& agent, TargetPosition& targetPosition)
	: AiAction(priority, expiryTime, "Arrive Action"), agent_(agent), target_position_(targetPosition)
{}
ArriveAction::~ArriveAction()
{}

bool ArriveAction::CanInterrupt() const
{
	return true;
}
bool ArriveAction::CanDoBoth(const AiAction& other) const
{
	return false;
}
void ArriveAction::Execute(float dt)
{
	auto output = MovementAlgorithms::Dynamic::ArrivePm(agent_.RigidBody(), { *(target_position_.target) }
		, agent_.MaxSpeed(), agent_.MaxLinearAcceleration(), 10, 5, 0.5);
	agent_.DynamicUpdate(dt, output);
	agent_.Orientation(RadiansToDegrees(atan2f(agent_.Velocity().y, agent_.Velocity().x)));
	if (agent_.Position().distance(*(target_position_.target)) <= 10)
	{
		delete target_position_.target;
		target_position_.target = nullptr;
	}
}