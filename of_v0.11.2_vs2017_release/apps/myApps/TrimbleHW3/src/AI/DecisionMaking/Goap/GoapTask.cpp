#include "AI/DecisionMaking/Goap/GoapTask.h"
#include "AI/DecisionMaking/Goap/GoapState.h"

GoapTask::GoapTask(std::string taskName, unsigned int preConditions, unsigned int addConditions
		, unsigned int delConditions, bool exactMatchConditions) : task_name_(taskName)
		, pre_conditions_(preConditions), add_conditions_(addConditions), del_conditions_(delConditions)
	, exact_match_conditions_(exactMatchConditions)
{}

GoapTask::~GoapTask() 
{
}

std::string GoapTask::Name() const
{
	return task_name_;
}

bool GoapTask::MeetsConditions(const GoapState& state) const
{
	if (exact_match_conditions_)
	{
		return (state.GetState() == pre_conditions_);
	}
	else
	{
		return ((state.GetState() & pre_conditions_) == pre_conditions_);
	}
}

void GoapTask::ApplyConditions(GoapState& state) const
{
	unsigned int newState = state.GetState();
	newState = newState ^ del_conditions_;
	newState = newState | add_conditions_;
	state.SetState(newState);
}

GoapState GoapTask::GetAppliedState(const GoapState& state) const
{
	GoapState nextState(state);
	ApplyConditions(nextState);
	return nextState;
}

GoapState GoapTask::GetConditionState() const
{
	return GoapState(pre_conditions_);
}

float GoapTask::GetHeuristic(const GoapState& state) const
{
	GoapState addOnlyState(state.GetState() | add_conditions_);
	return static_cast<float>(addOnlyState.NumSetBits());
}