#include "AI/DecisionMaking/Goap/GoapState.h"

GoapState::GoapState(unsigned int stateInfo, size_t maxBits) : state_info_(stateInfo), max_bits_(maxBits)
	, num_set_bits_(CountSetBits())
{
}

GoapState::~GoapState() 
{}

bool GoapState::GetBitState(int idx) const
{
	return (state_info_ & (1 << idx));
}

void GoapState::SetBitState(int idx, bool state)
{
	if(idx < max_bits_)
	{
		if (state)
		{
			if (!GetBitState(idx))
			{
				state_info_ += (1 << idx);
				num_set_bits_++;
			}
		}
		else
		{
			if (GetBitState(idx))
			{
				state_info_ -= (1 << idx);
				num_set_bits_--;
			}
		}
	}
}

void GoapState::SetState(unsigned int state)
{
	state_info_ = state;
	num_set_bits_ = CountSetBits();
}

unsigned int GoapState::GetState() const
{
	return state_info_;
}

bool GoapState::operator==(const GoapState& other) const
{
	return (state_info_ == other.state_info_);
}

bool GoapState::operator!=(const GoapState& other) const
{
	return (state_info_ != other.state_info_);
}

size_t GoapState::NumSetBits() const
{
	return num_set_bits_;
}

size_t GoapState::CountSetBits() const
{
	size_t numSetBits = 0;
	for (size_t i = 0; i < max_bits_; i++)
	{
		if (state_info_ & (1 << i))
		{
			numSetBits++;
		}
	}
	return numSetBits;
}