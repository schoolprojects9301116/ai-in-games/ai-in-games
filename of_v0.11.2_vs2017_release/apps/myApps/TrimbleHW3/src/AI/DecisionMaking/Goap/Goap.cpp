#include "AI/DecisionMaking/Goap/Goap.h"
#include "AI/DecisionMaking/Goap/GoapState.h"
#include "AI/DecisionMaking/Goap/GoapTask.h"

#include <queue>
#include <stack>

struct GoapPriority
{
	float priority;
	GoapTask task;
};

std::stack<AiAction*> Goap(const GoapState& startState, const GoapState& targetState
	, const std::vector<GoapTask> tasks, std::unordered_map<std::string, AiAction*> actionMap)
{
	std::stack<AiAction*> actionStack;
	if (startState == targetState)
	{
		// Return empty because we are already there (no actions needed)
		return actionStack;
	}
	auto compararator = [](const GoapPriority& taskA, const GoapPriority& taskB)
	{
		return taskA.priority > taskB.priority;
	};

	auto stateHash = [](const GoapState& state)
	{
		return std::hash<unsigned int>()(state.GetState());
	};

	std::priority_queue<GoapPriority, std::vector<GoapPriority>, decltype(compararator)> fringe(compararator);
	std::unordered_map<GoapState, GoapTask, decltype(stateHash)> cameFrom(8, stateHash);
	std::unordered_map<GoapState, float, decltype(stateHash)> visited(8, stateHash);
	visited.insert({ startState, 0 });
	for (auto& task : tasks)
	{
		if (task.MeetsConditions(startState))
		{
			fringe.push({ task.GetHeuristic(startState), task });
		}
	}

	GoapState currentState(startState);
	GoapState nextState(startState);

	while (!fringe.empty())
	{
		auto& currentTask = fringe.top();
		fringe.pop();
		currentState = currentTask.task.GetConditionState();
		nextState = currentTask.task.GetAppliedState(currentState);
		if (nextState == targetState)
		{
			// This current task gets us to the target state so we are done
			cameFrom.insert({ nextState, currentTask.task });
			break;
		}
		
		for (auto& task : tasks)
		{
			if (task.MeetsConditions(nextState))
			{
				float priority = visited[currentState];
				priority += task.GetHeuristic(currentState);
				if (visited.find(nextState) == visited.end() || visited.at(nextState) > priority)
				{
					fringe.push({ priority, task });
					visited[nextState] = priority;
					cameFrom.insert({ nextState, task });
				}
			}
		}
	}

	// No Order of tasks could be found
	if (nextState != targetState)
	{
		return actionStack;
	}

	// Get the backwards order of tasks to complete
	std::vector<GoapTask> tempTaskList;
	currentState = targetState;
	while (currentState != startState)
	{
		GoapTask taskToState = cameFrom.at(currentState);
		tempTaskList.push_back(taskToState);
		currentState = taskToState.GetConditionState();
	}

	// Place tasks into stack to get correct ordering
	for (GoapTask nextTask: tempTaskList)
	{
		actionStack.push(actionMap.at(nextTask.Name()));
	}
	return actionStack;
	
}