#include "AI/DecisionMaking/BehaviorTrees/BehaviorTreeSelectorNode.h"
#include <assert.h>

BehaviorTreeSelectorNode::BehaviorTreeSelectorNode(std::string nodeName) : BehaviorTreeNode(nodeName)
{}

BehaviorTreeSelectorNode::~BehaviorTreeSelectorNode() {}

AiAction* BehaviorTreeSelectorNode::Execute(BehaviorTreeTick& tick)
{
	if ((*active_child_)->Status() == BtNodeStatus::BT_INACTIVE)
	{
		(*active_child_)->Enter();
	}

	AiAction* action = (*active_child_)->Execute(tick);

	switch ((*active_child_)->Status())
	{
	// Succeeds if a single child does
	case BtNodeStatus::BT_SUCCESS:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_SUCCESS;
		break;

	//	 Fails if all children have falied otherwise move onto next child
	case BtNodeStatus::BT_FAILED:
		(*active_child_)->Exit();
		active_child_++;
		if(active_child_ == children_.end())
		{
			status_ = BtNodeStatus::BT_FAILED;
			break;
		}
		// fall into next case if not last child

	// Still waiting for result
	case BtNodeStatus::BT_RUNNING:
		status_ = BtNodeStatus::BT_RUNNING;
		break;

	// Error
	case BtNodeStatus::BT_ERROR:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_ERROR;
		break;

	default:
		assert(false);
		break;
	}

	return action;
}

//void BehaviorTreeSelectorNode::ExploreNode()
//{
//	if (status_ != AiActionStatus::RUNNING)
//	{
//		running_index_ = 0;
//	}
//
//	children_[running_index_].ExploreNode();
//
//	switch (children_[running_index_].Status())
//	{
//	// Succeeds if a single child succeeds
//	case AiActionStatus::SUCCESS:
//		status_ = AiActionStatus::SUCCESS;
//		break;
//
//	// Fails if all children have falied otherwise move onto next child
//	case AiActionStatus::FAILED:
//		running_index_++;
//		if (running_index_ == children_.size())
//		{
//			status_ = AiActionStatus::FAILED;
//			break;
//		}
//
//	// Still waiting for result
//	case AiActionStatus::RUNNING:
//		status_ = AiActionStatus::RUNNING;
//		break;
//
//	// Error or None (which should never occur) results in error
//	case AiActionStatus::ERROR:
//	case AiActionStatus::NONE:
//		status_ = AiActionStatus::ERROR;
//		break;
//
//	default:
//		break;
//	}
//}
