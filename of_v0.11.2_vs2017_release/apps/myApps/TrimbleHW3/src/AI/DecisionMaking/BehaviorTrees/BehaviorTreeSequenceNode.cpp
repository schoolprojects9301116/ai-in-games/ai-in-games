#include "AI/DecisionMaking/BehaviorTrees/BehaviorTreeSequenceNode.h"
#include <assert.h>

BehaviorTreeSequenceNode::BehaviorTreeSequenceNode(std::string nodeName) : BehaviorTreeNode(nodeName)
{}

BehaviorTreeSequenceNode::~BehaviorTreeSequenceNode()
{}

AiAction* BehaviorTreeSequenceNode::Execute(BehaviorTreeTick& tick)
{
	if ((*active_child_)->Status() == BtNodeStatus::BT_INACTIVE)
	{
		(*active_child_)->Enter();
	}

	AiAction* action = (*active_child_)->Execute(tick);

	switch ((*active_child_)->Status())
	{
	// Fails if a single child fails
	case BtNodeStatus::BT_FAILED:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_FAILED;
		break;

		// Succeeds if ALL children succeed
	case BtNodeStatus::BT_SUCCESS:
		(*active_child_)->Exit();
		active_child_++;
		if (active_child_ == children_.end())
		{
			status_ = BtNodeStatus::BT_SUCCESS;
			break;
		}
		// fall into next case if not last child

	// Still running
	case BtNodeStatus::BT_RUNNING:
		status_ = BtNodeStatus::BT_RUNNING;
		break;

		// Error occured
	case BtNodeStatus::BT_ERROR:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_ERROR;
		break;

	// Inavtive active_child_ should never occur
	default:
		assert(false);
		break;
	}

	return action;
}