#include "AI/DecisionMaking/BehaviorTrees/BehaviorTree.h"
#include <assert.h>

// =============================================================================================================
// ==																										  ==
// ==									  	BehaviorTreeNode												  ==
// ==																										  ==
// =============================================================================================================

BehaviorTreeNode::BehaviorTreeNode(std::string nodeName) : status_(BtNodeStatus::BT_INACTIVE), active_child_(children_.end())
	, node_name_(nodeName)
{}

BehaviorTreeNode::~BehaviorTreeNode() 
{
	for (size_t i = 0; i < children_.size(); i++)
	{
		delete children_[i];
	}
}

void BehaviorTreeNode::AddChild(BehaviorTreeNode* child)
{
	children_.push_back(child);
}

BtNodeStatus BehaviorTreeNode::Status() const
{
	return status_;
}

void BehaviorTreeNode::Enter()
{
	status_ = BtNodeStatus::BT_RUNNING;
	active_child_ = children_.begin();
}

void BehaviorTreeNode::Exit()
{
	status_ = BtNodeStatus::BT_INACTIVE;
	active_child_ = children_.end();
}

// =============================================================================================================
// ==																										  ==
// ==										 Behavior Tree													  ==
// ==																										  ==
// =============================================================================================================

BehaviorTree::BehaviorTree(BehaviorTreeNode* rootNode) : root_node_(rootNode)
{
	assert(rootNode != nullptr);
}

BehaviorTree::~BehaviorTree()
{
	delete root_node_;
}

AiAction* BehaviorTree::Execute()
{
	BehaviorTreeTick tick;
	if (root_node_->Status() == BtNodeStatus::BT_INACTIVE)
	{
		root_node_->Enter();
	}

	AiAction* action = root_node_->Execute(tick);

	if (root_node_->Status() != BtNodeStatus::BT_INACTIVE && root_node_->Status() != BtNodeStatus::BT_RUNNING)
	{
		root_node_->Exit();
	}

	return action;
}
