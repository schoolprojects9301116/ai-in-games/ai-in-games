#include "AI/DecisionMaking/BehaviorTrees/BehaviorTreeActionNode.h"
#include <assert.h>

BehaviorTreeActionNode::BehaviorTreeActionNode(std::string nodeName, AiAction* action)
	: BehaviorTreeNode(nodeName), action_(action), action_scheduled_(false)
{
	assert(action != nullptr);
}

BehaviorTreeActionNode::~BehaviorTreeActionNode()
{
	delete action_;
}

AiAction* BehaviorTreeActionNode::Execute(BehaviorTreeTick& tick)
{
	// If the action is not currently scheduled send it out for scheduling
	if (action_scheduled_)
	{
		// if its complete update status and reset bool
		if (action_->IsComplete())
		{
			action_scheduled_ = false;
			if (action_->Status() == AiActionStatus::ACTION_SUCCESS)
			{
				status_ = BtNodeStatus::BT_SUCCESS;
			}
			else if (action_->Status() == AiActionStatus::ACTION_FAILED)
			{
				status_ = BtNodeStatus::BT_FAILED;
			}
			else
			{
				status_ = BtNodeStatus::BT_ERROR;
			}
		}
		return nullptr;
	}
	else
	{
		action_scheduled_ = true;
		return action_;
	}
}

BehaviorTreeConditionNode::BehaviorTreeConditionNode(std::string nodeName, AiConditionAction* action) 
	: BehaviorTreeNode(nodeName), action_(action), action_scheduled_(false)
{
	assert(action != nullptr);
}

BehaviorTreeConditionNode::~BehaviorTreeConditionNode()
{
	delete action_;
}

AiAction* BehaviorTreeConditionNode::Execute(BehaviorTreeTick& tick)
{
	// If the action is not currently scheduled send it out for scheduling
	if (action_scheduled_)
	{
		// if its complete update status and reset bool
		if (action_->IsComplete())
		{
			action_scheduled_ = false;
			if(action_->Status() != AiActionStatus::ACTION_FAILED 
				&& action_->Status() != AiActionStatus::ACTION_SUCCESS)
			{
				status_ = BtNodeStatus::BT_ERROR;
			}
			else if (action_->GetResult())
			{
				status_ = BtNodeStatus::BT_SUCCESS;
			}
			else
			{
				status_ = BtNodeStatus::BT_FAILED;
			}
		}
		return nullptr;
	}
	else
	{
		action_scheduled_ = true;
		return action_;
	}
}