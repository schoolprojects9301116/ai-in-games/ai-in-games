#include "AI/DecisionMaking/BehaviorTrees/DecoratorNodes.h"
#include <assert.h>

BehaviorTreeInverterNode::BehaviorTreeInverterNode(std::string nodeName) : BehaviorTreeNode(nodeName)
{}

BehaviorTreeInverterNode::~BehaviorTreeInverterNode()
{}

void BehaviorTreeInverterNode::Exit()
{
	status_ = BtNodeStatus::BT_INACTIVE;
}

AiAction* BehaviorTreeInverterNode::Execute(BehaviorTreeTick& tick)
{
	if ((*active_child_)->Status() == BtNodeStatus::BT_INACTIVE)
	{
		(*active_child_)->Enter();
	}

	AiAction* action = (*active_child_)->Execute(tick);

	switch ((*active_child_)->Status())
	{
	case BtNodeStatus::BT_SUCCESS:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_FAILED;
		break;
	case BtNodeStatus::BT_FAILED:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_SUCCESS;
		break;
	case BtNodeStatus::BT_ERROR:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_ERROR;
		break;
	case BtNodeStatus::BT_RUNNING:
		break;
	default:
		assert(false);
		break;
	}
	return action;
}


BehaviorTreeUntilSuccessNode::BehaviorTreeUntilSuccessNode(std::string nodeName) : BehaviorTreeNode(nodeName)
{

}

BehaviorTreeUntilSuccessNode::~BehaviorTreeUntilSuccessNode()
{

}

void BehaviorTreeUntilSuccessNode::Exit()
{
	status_ = BtNodeStatus::BT_INACTIVE;
}

AiAction* BehaviorTreeUntilSuccessNode::Execute(BehaviorTreeTick& tick)
{
	if ((*active_child_)->Status() == BtNodeStatus::BT_INACTIVE)
	{
		(*active_child_)->Enter();
	}

	AiAction* action = (*active_child_)->Execute(tick);
	switch ((*active_child_)->Status())
	{
	case BtNodeStatus::BT_SUCCESS:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_SUCCESS;
		break;
	case BtNodeStatus::BT_FAILED:
		(*active_child_)->Exit();
	case BtNodeStatus::BT_RUNNING:
		status_ = BtNodeStatus::BT_RUNNING;
		break;
	case BtNodeStatus::BT_ERROR:
		(*active_child_)->Exit();
		status_ = BtNodeStatus::BT_ERROR;
		break;
	default:
		assert(false);
		break;
	}
	return action;
}