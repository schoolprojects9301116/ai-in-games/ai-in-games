#include "HW3/CustomBehaviorTreeActions.h"
#include "AI/Core/MovementAlgorithms.h"

TimerAction::TimerAction(int priority, float expiryTime, float timingThreshold, TargetTimingComponent& targetTimer) 
		: AiConditionAction(priority, expiryTime, "BT Timer Action"), timing_threshold_(timingThreshold)
	, target_timer_(targetTimer)
{}

TimerAction::~TimerAction()
{}

bool TimerAction::CanInterrupt() const
{
	return true;
}

bool TimerAction::CanDoBoth(const AiAction& other) const
{
	return true;
}

void TimerAction::Execute(float dt)
{	
	result_ = (target_timer_.ElapsedTimeOnCurrentTarget() >= timing_threshold_);
}


HasPathCondition::HasPathCondition(int priority, float expiryTime, std::stack<ofVec2f>& path) 
	: AiConditionAction(priority, expiryTime, "Has Path Condition"), path_(path)
{}

HasPathCondition::~HasPathCondition()
{}

bool HasPathCondition::CanInterrupt() const
{
	return false;
}

bool HasPathCondition::CanDoBoth(const AiAction& other) const
{
	return false;
}
void HasPathCondition::Execute(float dt)
{
	result_ = path_.size() > 0;
}


ProximityCheck::ProximityCheck(int priority, float expiryTime, float threshold, const AiAgentComponent& agent
			, std::stack<ofVec2f>& path)
		: AiConditionAction(priority, expiryTime, "BT Proximity Check"), threshold_(threshold), agent_(agent)
	, path_(path)
{}

ProximityCheck::~ProximityCheck()
{}

bool ProximityCheck::CanInterrupt() const
{
	return false;
}

bool ProximityCheck::CanDoBoth(const AiAction& other) const
{
	return false;
}

void ProximityCheck::Execute(float dt)
{
	if (path_.empty())
	{
		result_ = false;
	}
	else
	{
		result_ = (agent_.Position().distance(path_.top()) <= threshold_);
	}
}


GetTargetLocationAction::GetTargetLocationAction(int priority, float expiryTime, AiAgentComponent* target
			, TargetPosition& targetPosition, std::stack<ofVec2f>& path) 
		: AiAction(priority, expiryTime, "BT Get Target Location"), target_(target), target_position_(targetPosition)
	, path_(path)
{

}

GetTargetLocationAction::~GetTargetLocationAction()
{}

bool GetTargetLocationAction::CanInterrupt() const
{
	return false;
}

bool GetTargetLocationAction::CanDoBoth(const AiAction& other) const
{
	return false;
}

void GetTargetLocationAction::Execute(float dt)
{
	if (target_position_.target == nullptr)
	{
		target_position_.target = new ofVec2f();
	}
	*(target_position_.target) = target_->Position();
	while (path_.size() > 0)
	{
		path_.pop();
	}
}

ArriveOnPathAction::ArriveOnPathAction(int priority, float expiryTime, AiAgentComponent& agent
		, const std::stack<ofVec2f>& path) : AiAction(priority, expiryTime, "BT Arrive Action"), agent_(agent)
	, path_(path)
{}

ArriveOnPathAction::~ArriveOnPathAction()
{}

bool ArriveOnPathAction::CanInterrupt() const
{
	return true;
}

bool ArriveOnPathAction::CanDoBoth(const AiAction& other) const
{
	return true;
}

void ArriveOnPathAction::Execute(float dt)
{
	if (path_.size() > 0)
	{
		auto output = MovementAlgorithms::Dynamic::ArrivePm(agent_.RigidBody(), { path_.top() }, agent_.MaxSpeed()
			, agent_.MaxLinearAcceleration(), 10, 5, 0.5);
		agent_.DynamicUpdate(dt, output);
		agent_.Orientation(RadiansToDegrees(atan2f(agent_.Velocity().y, agent_.Velocity().x)));
	}
	else
	{
		agent_.SetVelocity({ 0, 0 });
	}
}

ReducePathAction::ReducePathAction(int priority, float expiryTime, std::stack<ofVec2f>& path)
	: AiAction(priority, expiryTime, "BT Reduce Path Action"), path_(path)
{}

ReducePathAction::~ReducePathAction()
{}

bool ReducePathAction::CanInterrupt() const
{
	return false;
}

bool ReducePathAction::CanDoBoth(const AiAction& other) const
{
	return false;
}

void ReducePathAction::Execute(float dt)
{
	if (path_.size() > 0)
	{
		path_.pop();
	}
}