#include "AI/DecisionMaking/AiActionManager.h"
#include <iostream>

AiActionManager::AiActionManager(game_engine::GameObject* gameObject) 
	: game_engine::Component(gameObject) {}

void AiActionManager::ScheduleAction(AiAction* action)
{
	auto tempQueue(pending_queue_);
	bool actionExists = false;
	while (!tempQueue.empty())
	{
		auto& pendingAction = pending_queue_.top();
		pending_queue_.pop();
		if (&pendingAction == &action)
		{
			actionExists = true;
			break;
		}
	}
	if (!actionExists)
	{
		for (size_t i = 0; i < active_actions_.size(); i++)
		{
			if (&action == &active_actions_[i])
			{
				actionExists = true;
				break;
			}
		}
	}
	if (!actionExists)
	{
		action->ScheduleAction();
		pending_queue_.push(action);
	}
}

AiActionManager::~AiActionManager() {}

void AiActionManager::Update(float dt)
{
	if (pending_queue_.size() > 0)
	{
		UpdateActionQueuedTime(dt);
		CheckInterrupts();
		PromoteQueuedActionsToActive();
	}
	if (active_actions_.size() > 0)
	{
		RunActiveActions(dt);
	}
}

void AiActionManager::UpdateActionQueuedTime(float dt)
{
	auto tempQueue(pending_queue_);
	while (!tempQueue.empty())
	{
		auto& action = tempQueue.top();
		action->UpdateQueuedTime(dt);
		tempQueue.pop();
	}
}

void AiActionManager::CheckInterrupts()
{
	auto & pendingAction = pending_queue_.top();
	std::vector<size_t> interruptIndicies;
	bool interruptActiveActions = true;
	for (AiAction* activeAction : active_actions_)
	{
		if (activeAction->Priority() >= pendingAction->Priority() || !activeAction->CanInterrupt())
		{
			interruptActiveActions = false;
		}
	}
	if (interruptActiveActions)
	{
		for (AiAction* activeAction : active_actions_)
		{
			activeAction->TerminateAction();
		}
		active_actions_.clear();
	}
}

void AiActionManager::PromoteQueuedActionsToActive()
{
	while (!pending_queue_.empty())
	{
		auto& pendingAction = pending_queue_.top();
		bool shouldRun = true;
		for (AiAction* activeAction : active_actions_)
		{
			if (!activeAction->CanDoBoth(*pendingAction))
			{
				shouldRun = false;
				break;
			}
		}
		if (shouldRun)
		{
			active_actions_.push_back(pendingAction);
			pending_queue_.pop();
		}
		else
		{
			break;
		}
	}
}

void AiActionManager::RunActiveActions(float dt)
{
	std::vector<size_t> completedIndices;
	for (size_t i = 0; i < active_actions_.size(); i++)
	{
		//std::cout << "Executing action with name: " << active_actions_[i]->Name() << std::endl;
		active_actions_[i]->Execute(dt);
		active_actions_[i]->CompleteAction();
	}
	active_actions_.clear();
}