#include "Hw1/AiStateTracker.h"

namespace Hw1
{
	AiStateTracker* AiStateTracker::instance_ = nullptr;

	AiStateTracker* AiStateTracker::Instance()
	{
		if (instance_ == nullptr)
		{
			instance_ = new AiStateTracker();
		}
		return instance_;
	}

	void AiStateTracker::Shutdown()
	{
		if (instance_ != nullptr)
		{
			delete instance_;
		}
	}

	AiStateTracker::AiStateTracker() : current_state_(Hw1State::kKinematicMotion){}
	AiStateTracker::~AiStateTracker() {}

	void AiStateTracker::SetKeyStatus(int key, bool isPressed)
	{
		if (isPressed)
		{
			key_states_.insert(key);
			switch (key)
			{
			case 49:
				current_state_ = Hw1State::kKinematicMotion;
				break;
			case 50:
				current_state_ = Hw1State::kSeek;
				break;
			case 51:
				current_state_ = Hw1State::kWander;
				break;
			case 52:
				current_state_ = Hw1State::kFlock;
				break;
			default:
				break;
			}
		}
		else
		{
			if (key_states_.find(key) != key_states_.end())
			{
				key_states_.erase(key);
			}
		}
	}

	void AiStateTracker::SetMouseClickPosition(int x, int y)
	{
		last_mouse_click_position_ = { (float)x, (float)y };
		mouse_clicked_ = true;
	}

	ofVec2f AiStateTracker::GetMouseClickPosition()
	{
		return last_mouse_click_position_;
	}

	bool AiStateTracker::HasMouseBeenClickedEver()
	{
		return mouse_clicked_;
	}

	bool AiStateTracker::GetKeyPressed(int key)
	{
		return key_states_.find(key) != key_states_.end();
	}

	Hw1State AiStateTracker::GetState() 
	{
		return current_state_;
	}

}