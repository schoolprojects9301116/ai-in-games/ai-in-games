#include "Engine/Rendering/RenderComponent.h"

namespace game_engine
{
namespace rendering
{
	RenderComponent::RenderComponent(GameObject* gameObject) : game_object(gameObject) {}
	void RenderComponent::SetDrawFunction(DrawFunction drawFunction) { draw_function_ = drawFunction; }
	void RenderComponent::Draw()
	{
		if (draw_function_ != nullptr)
		{
			draw_function_(game_object);
		}
	}
}
}