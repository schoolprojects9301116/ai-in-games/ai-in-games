#include "Engine/Maps/MapGenerator.h"
#include "Engine/Core/GameObject.h"
#include "../../ofApp.h"
#include <fstream>
#include "ofVec2f.h"
#include "Engine/Core/RayCast.h"

//#define DRAW_GRID

namespace game_engine
{
namespace maps
{

	AbstractionScheme* AbstractionScheme::current_pathfinding_scheme_ = nullptr;
	
	void DrawLevelMap(GameObject* gameObject)
	{
		LevelMap* levelMap = gameObject->GetCompenent<LevelMap>();
		if (levelMap != nullptr)
		{
			levelMap->DrawLevel();
		}
	}

	LevelMap::LevelMap(GameObject* gameObject, ofColor obstacleColor)
		: Component(gameObject), obstacle_color_(obstacleColor), scheme_(nullptr)
	{
		gameObject->SetDrawFunction(&DrawLevelMap);
	}

	LevelMap::~LevelMap() 
	{
		delete scheme_;
	}

	bool LevelMap::DetectCollision(ofVec2f position, float offsetDistance) const
	{
		for (const auto& obstacle : obstacles_)
		{
			if (position.x > obstacle.position.x - offsetDistance
				&& position.x < obstacle.position.x + obstacle.width  + offsetDistance
				&& position.y > obstacle.position.y - offsetDistance
			&& position.y < obstacle.position.y + obstacle.height + offsetDistance)
			{
				return true;
			}
		}
		return false;
	}

	LevelMap* LevelMap::CreateLevelMap(GameObject* gameObject, std::string fileName, ofColor obstacleColor
		, size_t abstractionRows, size_t abtractionColumns)
	{
		LevelMap* map = nullptr;
		std::ifstream mapFile;
		mapFile.open(fileName);
		if (mapFile.is_open())
		{
			map = new LevelMap(gameObject, obstacleColor);
			size_t numColumns;
			size_t numRows;
			float xScale;
			float yScale;

			std::string line;
			std::getline(mapFile, line);
			numColumns = std::stoi(line);
			std::getline(mapFile, line);
			numRows = std::stoi(line);
			xScale = ((float)ofGetWindowWidth() / (float)numColumns);
			yScale = ((float)ofGetWindowHeight() / (float)numRows);
			size_t currentY = 0;
			while (std::getline(mapFile, line))
			{
				bool isMakingObstacle = false;
				size_t obstacleStartLocation;
				MapObstacle* lastObstacle = nullptr;
				for (size_t i = 0; i < numColumns; i++)
				{
					if (line[i] == 'x')
					{
						if (!isMakingObstacle)
						{
							isMakingObstacle = true;
							map->obstacles_.push_back(MapObstacle({ {i * xScale, currentY * yScale}, xScale, yScale }));
							obstacleStartLocation = i;
							lastObstacle = &map->obstacles_[map->obstacles_.size() - 1];
						}
						if (i == numColumns - 1)
						{
							isMakingObstacle = false;
							lastObstacle->width = (numColumns - obstacleStartLocation) * xScale;
						}
					}
					else if (line[i] == '0')
					{
						if (isMakingObstacle)
						{
							isMakingObstacle = false;
							lastObstacle->width = (i - obstacleStartLocation) * xScale;
						}
					}
					else
					{
						delete map;
						return nullptr;
					}
				}
				currentY++;
			}
			map->scheme_ = new AbstractionScheme(*map, abstractionRows, abtractionColumns);
		}
		return map;
	}

	void LevelMap::Update(float dt)
	{	}

	std::stack<ofVec2f> LevelMap::Pathfind(ofVec2f start, ofVec2f goal)
	{
		auto backwardsPath = scheme_->Pathfind(start, goal);
		std::stack<ofVec2f> stackPath;
		for (size_t i = 0; i < backwardsPath.size(); i++)
		{
			stackPath.push(backwardsPath[i]);
		}
		return stackPath;
	}

	AbstractionScheme::AbstractionScheme(const LevelMap & map, size_t numRows, size_t numColumns) :
			num_columns_(numColumns), num_rows_(numRows)
			, num_nodes_(numRows * numColumns), x_scale_((float)ofGetWindowWidth() / (float)numColumns)
			, y_scale_((float)ofGetWindowHeight() / (float)numRows), graph_(numRows * numColumns, numRows * numColumns)
		, node_world_positions_(new ofVec2f[num_columns_ * num_rows_])
	{
		size_t currentNode = 0;
		for (size_t y = 0; y < numRows; y++)
		{
			for (size_t x = 0; x < numColumns; x++)
			{
				node_world_positions_[currentNode] = ofVec2f(x_scale_ * ((float)x + 0.5f), y_scale_ * ((float)y + 0.5f));
				if (!map.DetectCollision(node_world_positions_[currentNode], 25))
				{
					bool leftClear = false;
					if (x != 0 && !map.DetectCollision(node_world_positions_[currentNode-1], 15))
					{
						leftClear = true;
						graph_(currentNode, currentNode - 1) = 1;
						graph_(currentNode - 1, currentNode) = 1;
					}
					if (y != 0 && !map.DetectCollision(node_world_positions_[currentNode - num_columns_], 15))
					{
						if (x != 0 && leftClear && !map.DetectCollision(node_world_positions_[currentNode - num_columns_ - 1], 15))
						{
							graph_(currentNode, currentNode - num_columns_ - 1) = 3;
							graph_(currentNode - num_columns_ - 1, currentNode) = 3;
							graph_(currentNode - 1, currentNode - num_columns_) = 3;
							graph_(currentNode - num_columns_, currentNode - 1) = 3;
						}
						graph_(currentNode, currentNode - num_columns_) = 1;
						graph_(currentNode - num_columns_, currentNode) = 1;

					}

				}
				currentNode++;
			}
		}
	}

	AbstractionScheme::~AbstractionScheme()
	{
		delete [] node_world_positions_;
	}

	const AbstractionScheme* AbstractionScheme::CurrentPathfindingScheme()
	{
		return current_pathfinding_scheme_;
	}


	size_t AbstractionScheme::ToGraphSpace(ofVec2f position) const
	{
		size_t xPos = (position.x / x_scale_);
		size_t yPos = (position.y / y_scale_);
		return (yPos * num_columns_) + xPos;
	}

	ofVec2f AbstractionScheme::ToWorldSpace(size_t node) const
	{
		return node_world_positions_[node];
	}

	float ReturnZero(size_t, size_t)
	{
		return 0.0f;
	}

	float ManhattanDistance(size_t nodeA, size_t nodeB)
	{
		const AbstractionScheme* scheme = AbstractionScheme::CurrentPathfindingScheme();
		auto posNodeA = scheme->NodeGridPos(nodeA);
		auto posNodeB = scheme->NodeGridPos(nodeB);
		float distance = std::abs((float)posNodeB.first - (float)posNodeA.first);
		distance += std::abs((float)posNodeB.second - (float)posNodeA.second);
		return distance;
	}

	float GeometricDistance(size_t nodeA, size_t nodeB)
	{
		const AbstractionScheme* scheme = AbstractionScheme::CurrentPathfindingScheme();
		auto posNodeA = scheme->NodeGridPos(nodeA);
		auto posNodeB = scheme->NodeGridPos(nodeB);
		float distance = posNodeB.first - posNodeA.first;
		distance *= distance;
		distance += (posNodeB.second - posNodeA.second) * (posNodeB.second - posNodeA.second);
		return sqrt(distance);
	}

	float ConstantGuess(size_t nodeA, size_t nodeB)
	{
		return 5;
	}

	std::pair<size_t, size_t> AbstractionScheme::NodeGridPos(size_t node)const
	{
		return std::pair<size_t, size_t>(node % num_columns_, node / num_columns_);
	}

	std::vector<ofVec2f> AbstractionScheme::Pathfind(ofVec2f Start, ofVec2f Goal)
	{
		current_pathfinding_scheme_ = this;
		size_t startNode = ToGraphSpace(Start);
		size_t endNode = ToGraphSpace(Goal);
		auto heuristic = [](size_t, size_t) {return 0; };
		//std::vector<size_t> nodePath = Pathfinding::AStar(graph_, startNode, endNode, &ManhattanDistance);
		//std::vector<size_t> nodePath = Pathfinding::AStar(graph_, startNode, endNode, &GeometricDistance);
		std::vector<size_t> nodePath = Pathfinding::AStar(graph_, startNode, endNode, &ConstantGuess);
		std::vector<ofVec2f> positionPath(nodePath.size());
		for (size_t i = 0; i < nodePath.size(); i++)
		{
			positionPath[i] = ToWorldSpace(nodePath[i]);
		}
		return positionPath;
	}

	void LevelMap::DrawLevel()
	{
		ofSetColor(obstacle_color_);
		for (auto& obstacle : obstacles_)
		{
			ofDrawRectangle(obstacle.position.x, obstacle.position.y, obstacle.width, obstacle.height);
		}
#ifdef DRAW_GRID
		scheme_->DrawGrid();
#endif // DRAW_GRID

	}

	void AbstractionScheme::DrawGrid()
	{
		ofSetColor(ofColor::white);
		for (size_t x = 0; x < num_columns_; x++)
		{
			ofDrawLine(x * x_scale_, 0, x * x_scale_, ofGetWindowHeight());
		}
		for (size_t y = 0; y < num_rows_; y++)
		{
			ofDrawLine(0, y * y_scale_, ofGetWindowWidth(), y * y_scale_);
		}
	}
}
}