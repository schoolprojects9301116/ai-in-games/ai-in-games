#include <random>
#include "Engine/Math/Random.h"

namespace Engine
{
	float RandomBinomial()
	{
		return RandomFloat() - RandomFloat();
	}

	float RandomFloat()
	{
		return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
	}
}