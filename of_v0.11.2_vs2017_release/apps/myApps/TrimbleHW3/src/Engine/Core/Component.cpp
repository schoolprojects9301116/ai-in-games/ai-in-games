#include "Engine/Core/Component.h"

namespace game_engine
{
	Component::Component(GameObject* gameObject) : gameObject_(gameObject)
	{}

	Component::~Component() {}

	GameObject* Component::GetGameObject()
	{
		return gameObject_;
	}

}