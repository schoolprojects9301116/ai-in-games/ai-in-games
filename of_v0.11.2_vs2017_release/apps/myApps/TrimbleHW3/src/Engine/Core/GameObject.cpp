#include "Engine/Core/GameObject.h"
#include "Engine/Core/Component.h"

namespace game_engine
{
	GameObject::GameObject() : render_component_(this), is_active_(true) {}
	GameObject::~GameObject()
	{
		for (Component* componentPointer : components_)
		{
			delete componentPointer;
		}
		components_.clear();
	}

	void GameObject::AddComponent(Component* component)
	{
		if (std::find(components_.begin(), components_.end(), component) == components_.end())
		{
			components_.push_back(component);
		}
	}

	void GameObject::UpdateAllComponents(float dt)
	{
		for (Component* componentPointer : components_)
		{
			componentPointer->Update(dt);
		}
	}

	void GameObject::SetDrawFunction(rendering::DrawFunction drawFunction)
	{
		render_component_.SetDrawFunction(drawFunction);
	}

	void GameObject::Draw()
	{
		render_component_.Draw();
	}

	bool GameObject::Active() const
	{
		return is_active_;
	}

	void GameObject::Active(bool isActive)
	{
		is_active_ = isActive;
	}

}