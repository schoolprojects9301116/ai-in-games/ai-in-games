#include "ofVec2f.h"
#include "Engine/Maps/MapGenerator.h"

namespace game_engine
{
	bool RayCast(const maps::LevelMap& map, ofVec2f startPosition, ofVec2f direction, float travelDistance, float stepDistance)
	{
		ofVec2f stepVector = direction.getNormalized() * stepDistance;
		ofVec2f currentPosition = startPosition;
		float distanceTraveled = 0.0f;
		while (distanceTraveled < travelDistance)
		{
			if (map.DetectCollision(currentPosition))
			{
				return true;
			}
			currentPosition += stepVector;
			distanceTraveled += stepDistance;
		}
		return false;
	}

	bool RayCast(const maps::LevelMap& map, ofVec2f startPosition, ofVec2f endPosition, float stepDistance)
	{
		ofVec2f directionVector = endPosition - startPosition;
		return RayCast(map, startPosition, directionVector, directionVector.length(), stepDistance);
	}
}