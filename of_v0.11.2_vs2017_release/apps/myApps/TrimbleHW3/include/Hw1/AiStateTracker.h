#pragma once

#include <unordered_set>
#include "ofVec2f.h"

namespace Hw1
{

enum class Hw1State
{
	kKinematicMotion,
	kSeek,
	kWander,
	kFlock
};

class AiStateTracker
{
public:
	static AiStateTracker* Instance();
	static void Shutdown();
	~AiStateTracker();

	void SetKeyStatus(int key, bool isPressed);
	void SetMouseClickPosition(int x, int y);
	ofVec2f GetMouseClickPosition();
	bool GetKeyPressed(int key);
	Hw1State GetState();
	bool HasMouseBeenClickedEver();

private:
	AiStateTracker();
	static AiStateTracker* instance_;
	Hw1State current_state_;
	std::unordered_set<int> key_states_;
	ofVec2f last_mouse_click_position_;
	bool mouse_clicked_ = false;
};
}