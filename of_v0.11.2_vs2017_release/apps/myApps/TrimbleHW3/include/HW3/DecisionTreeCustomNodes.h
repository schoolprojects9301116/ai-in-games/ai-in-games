#pragma once
#include "AI/DecisionMaking/DecisionTrees/DecisionTreeNode.h"
#include "ofVec2f.h"
#include "AI/Core/AiAgentComponent.h"
#include <stack>
#include "Engine/Core/Component.h"
#include "AI/DecisionMaking/DecisionContainers.h"
#include "Engine/Maps/MapGenerator.h"

DecisionTreeNode* GenerateCustomTree(game_engine::GameObject* gameObject, TargetPosition& targetPosition
	, AiAgentComponent& agent, std::stack<ofVec2f>& targetPath, game_engine::maps::LevelMap& map, float maxX, float maxY);

// =============================================================================================================
// ==																										  ==
// ==										 Custom Branch Conditions										  ==
// ==																										  ==
// =============================================================================================================
class ValidTargetCondition final : public DecisionTreeBranchCondition
{
public:
	ValidTargetCondition(TargetPosition& targetPosition);
	virtual ~ValidTargetCondition();
	virtual bool CheckCondition() override;

private:
	TargetPosition& target_position_;
};

class ProximityCondition final : public DecisionTreeBranchCondition
{
public:
	ProximityCondition(AiAgentComponent& agent, TargetPosition& targetPosition, float proximityThreshold);
	virtual ~ProximityCondition();
	virtual bool CheckCondition() override;

private:
	AiAgentComponent& agent_;
	TargetPosition& target_position_;
	float proximity_threshold_;
};

class RemainingStepsCondition final : public DecisionTreeBranchCondition
{
public:
	RemainingStepsCondition(std::stack<ofVec2f>& pathStack, int stepThreshold);
	virtual ~RemainingStepsCondition();
	virtual bool CheckCondition() override;

private:
	std::stack<ofVec2f>& path_stack_;
	int step_threshold_;
};

class TargetTimingComponent : public game_engine::Component
{
public:
	TargetTimingComponent(game_engine::GameObject* gameObject, TargetPosition& targetPosition);
	virtual ~TargetTimingComponent();
	virtual void Update(float dt) override;

	float ElapsedTimeOnCurrentTarget();

private:
	TargetPosition& target_position_;
	ofVec2f last_target_;
	float elapsed_time_;
};

class TargetTimingCondition final : public DecisionTreeBranchCondition
{
public:
	TargetTimingCondition(TargetTimingComponent* timingComponent, float timingThreshold);
	virtual ~TargetTimingCondition();
	virtual bool CheckCondition() override;

private:
	TargetTimingComponent* timing_component_;
	float timing_threshold_;
};

class SpeedThresholdCondition final : public DecisionTreeBranchCondition
{
public:
	SpeedThresholdCondition(AiAgentComponent* agent, float speedThreshold);
	virtual ~SpeedThresholdCondition();
	virtual bool CheckCondition() override;

private:
	AiAgentComponent* agent_;
	float speed_threshold_;
};

class RandomCondition final : public DecisionTreeBranchCondition
{
public:
	RandomCondition(float probability);
	virtual ~RandomCondition();
	virtual bool CheckCondition() override;

private:
	float probablity_;
};