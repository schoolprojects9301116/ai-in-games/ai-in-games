#pragma once
#include "AI/DecisionMaking/AiAction.h"
#include "HW3/DecisionTreeCustomNodes.h"

class TimerAction : public AiConditionAction
{
public:
	TimerAction(int priority, float expiryTime, float timingThreshold, TargetTimingComponent& targetTimer);
	~TimerAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	float timing_threshold_;
	TargetTimingComponent& target_timer_;
};

class HasPathCondition : public AiConditionAction
{
public:
	HasPathCondition(int priority, float expiryTime, std::stack<ofVec2f>& path);
	~HasPathCondition();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	std::stack<ofVec2f>& path_;
};

class ProximityCheck : public AiConditionAction
{
public:
	ProximityCheck(int priority, float expiryTime, float threshold, const AiAgentComponent& agent
		, std::stack<ofVec2f>& path);
	~ProximityCheck();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	float threshold_;
	const AiAgentComponent& agent_;
	const std::stack<ofVec2f>& path_;
};

// =================================== ACTIONS ==================================

class GetTargetLocationAction : public AiAction
{
public:
	GetTargetLocationAction(int priority, float expiryTime, AiAgentComponent* target, TargetPosition& targetPosition
		, std::stack<ofVec2f>& path);
	~GetTargetLocationAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	AiAgentComponent* target_;
	TargetPosition& target_position_;
	std::stack<ofVec2f>& path_;
};

class ArriveOnPathAction : public AiAction
{
public:
	ArriveOnPathAction(int priority, float expiryTime, AiAgentComponent& agent, const std::stack<ofVec2f>& path);
	~ArriveOnPathAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	AiAgentComponent& agent_;
	const std::stack<ofVec2f>& path_;
};

class ReducePathAction : public AiAction
{
public:
	ReducePathAction(int priority, float expiryTime, std::stack<ofVec2f>& path);
	~ReducePathAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	std::stack<ofVec2f>& path_;
};