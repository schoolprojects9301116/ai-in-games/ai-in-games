#pragma once
#include "HW3/DecisionTreeCustomNodes.h"
#include "Engine/Core/Component.h"
#include "AI/Core/AiAgentComponent.h"
#include "Engine/Maps/MapGenerator.h"
#include "AI/DecisionMaking/DecisionTrees/DecisionTreeNode.h"
#include "AI/DecisionMaking/AiActionManager.h"
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTree.h"
#include "AI/DecisionMaking/DecisionContainers.h"
#include "AI/DecisionMaking/Goap/GoapTask.h"

class AiControllerHW3 : public game_engine::Component
{
public:
	AiControllerHW3(game_engine::GameObject* gameObject, AiAgentComponent* agent, game_engine::maps::LevelMap* map);
	virtual ~AiControllerHW3();
	virtual void Update(float dt);
protected:
	AiAgentComponent* agent_;
	game_engine::maps::LevelMap* map_;
};

class AiPlayerController : public AiControllerHW3
{
public:
	AiPlayerController(game_engine::GameObject* gameObject, AiAgentComponent* agent, game_engine::maps::LevelMap* map
		, AiActionManager& manager);
	virtual ~AiPlayerController();
	virtual void Update(float dt);

	void DrawTarget();

private:
	DecisionTreeNode* decision_tree_;
	TargetPosition target_position_;
	std::stack<ofVec2f> target_path_;
	AiAction* current_action_;
	AiActionManager& manager_;
	std::vector<GoapTask> goap_tasks_;
	std::unordered_map<std::string, AiAction*> goap_action_map_;
};

class AiMonsterController : public AiControllerHW3
{
public:
	AiMonsterController(game_engine::GameObject* gameObject, AiAgentComponent* agent, AiAgentComponent* player,
		game_engine::maps::LevelMap* map, AiActionManager& manager);
	virtual ~AiMonsterController();
	virtual void Update(float dt) override;

	void DrawTarget();

private:
	TargetPosition target_position_;
	TargetTimingComponent target_timer_;
	BehaviorTree* behavior_tree_;
	AiActionManager& manager_;
	std::stack<ofVec2f> target_path_;
};