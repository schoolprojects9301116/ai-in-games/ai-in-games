#pragma once

#include <vector>
#include "ofVec2f.h"
#include "Engine/Core/Component.h"
#include "ofColor.h"
#include "AI/Pathfinding/Graph.h"
#include <vector>
#include <stack>

namespace game_engine
{
namespace maps
{
	struct MapObstacle
	{
		ofVec2f position;
		float width;
		float height;
	};

	class LevelMap;

	class AbstractionScheme
	{
	public:
		AbstractionScheme(const LevelMap& owner, size_t numRows, size_t numColumns);
		~AbstractionScheme();
		size_t ToGraphSpace(ofVec2f position) const;
		ofVec2f ToWorldSpace(size_t node) const;
		std::vector<ofVec2f> Pathfind(ofVec2f Start, ofVec2f Goal);
		std::pair<size_t, size_t> NodeGridPos(size_t node) const;
		static const AbstractionScheme* CurrentPathfindingScheme();
		void DrawGrid();

	private:
		size_t num_columns_;
		size_t num_rows_;
		size_t num_nodes_;
		float x_scale_;
		float y_scale_;
		Pathfinding::Array2D graph_;
		ofVec2f* node_world_positions_;
		static AbstractionScheme* current_pathfinding_scheme_;
	};

	class LevelMap : public Component
	{
	public:
		static LevelMap* CreateLevelMap(GameObject* gameObject, std::string fileName, ofColor obstacleColor
			, size_t abstractionRows, size_t abtractionColumns);
		virtual ~LevelMap();
		virtual void Update(float dt);
		void DrawLevel();
		bool DetectCollision(ofVec2f position, float offsetDistance = 0.0f) const;
		std::stack<ofVec2f> Pathfind(ofVec2f start, ofVec2f goal);

	private:
		LevelMap(GameObject* gameObject, ofColor obstacleColor);
		std::vector<MapObstacle> obstacles_;
		ofColor obstacle_color_;
		AbstractionScheme* scheme_;
	};

}
}