#pragma once
#include <vector>
#include "Engine/Rendering/RenderComponent.h"

namespace game_engine
{

	class Component;

	class GameObject
	{
	public:
		GameObject();
		~GameObject();
		
		void AddComponent(Component* component);
		void UpdateAllComponents(float dt);
		void Draw();
		void SetDrawFunction(rendering::DrawFunction drawFunction);
		bool Active() const;
		void Active(bool);


		template<typename T>
		T* GetCompenent()
		{
			for (Component* componentPointer : components_)
			{
				T* castedPointer = dynamic_cast<T*>(componentPointer);
				if (castedPointer != nullptr)
				{
					return castedPointer;
				}
			}
			return nullptr;
		}


	private:
		std::vector<Component*> components_;
		rendering::RenderComponent render_component_;
		bool is_active_;
	};
}