#pragma once

namespace game_engine
{
	class GameObject;

	class Component
	{
	public:
		Component(GameObject* gameObject);
		virtual ~Component() = 0;

		virtual void Update(float dt) = 0;

		GameObject* GetGameObject();

	private:
		GameObject* gameObject_;
	};
}