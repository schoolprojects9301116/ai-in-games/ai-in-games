#pragma once
#include "Engine/Maps/MapGenerator.h"
#include "ofVec2f.h"

namespace game_engine
{
	bool RayCast(const maps::LevelMap& map, ofVec2f startPosition, ofVec2f endPosition, float stepDistance = 0.5f);
	bool RayCast(const maps::LevelMap& map, ofVec2f startPosition, ofVec2f direction, float travelDistance, float stepDistance = 0.5f);
}