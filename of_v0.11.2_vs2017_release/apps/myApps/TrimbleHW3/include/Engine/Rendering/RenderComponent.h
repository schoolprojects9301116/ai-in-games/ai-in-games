#pragma once

namespace game_engine
{

	class GameObject;

namespace rendering
{
	using DrawFunction = void(*)(GameObject*);

	class RenderComponent
	{
	public:
		RenderComponent(GameObject* gameObject);
		void Draw();
		void SetDrawFunction(DrawFunction drawFunction);

	private:
		DrawFunction draw_function_ = nullptr;
		GameObject* game_object;
	};
}
}