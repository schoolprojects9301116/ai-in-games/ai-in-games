#pragma once
#include "Engine/Core/Component.h"
#include "AI/Core/AiAgentComponent.h"
#include "Engine/Maps/MapGenerator.h"

class AiController : public game_engine::Component
{
public:
	AiController(game_engine::GameObject* gameObject, AiAgentComponent* agent, game_engine::maps::LevelMap* map);
	virtual ~AiController();
	virtual void Update(float dt);

private:
	AiAgentComponent* agent_;
	game_engine::maps::LevelMap* map_;
	ofVec2f target_position_;
	ofVec2f last_mouse_position_;
	std::stack<ofVec2f> follow_path_;
};