#ifndef RIGID_BODY_2D_H
#define RIGID_BODY_2D_H

#include "ofVec2f.h"

float DegreesToRadians(float degrees);
float RadiansToDegrees(float radians);

struct RigidBody2D
{
	ofVec2f position;
	ofVec2f velocity;
	float orientation;
	float rotation;
	float mass = 0.0f;
};

#endif // !RIGID_BODY_2D_H
