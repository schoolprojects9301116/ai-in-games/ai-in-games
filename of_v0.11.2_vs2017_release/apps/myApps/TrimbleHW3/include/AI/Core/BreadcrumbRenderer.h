#pragma once
#include "ofVec2f.h"

namespace Engine
{

	class BreadcrumbRenderer
	{
	public:
		static BreadcrumbRenderer& Instance();
		~BreadcrumbRenderer();

		void Render();
		void AddCrumb(ofVec2f crumb);
		void RemoveCrumb(ofVec2f crumb);

	private:
		BreadcrumbRenderer();
		static BreadcrumbRenderer* instance_;
		std::vector<ofVec2f> crumbLocations_;
	};

}