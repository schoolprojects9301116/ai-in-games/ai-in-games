#pragma once
#include "ofVec2f.h"

struct KinematicSteeringOutput
{
	ofVec2f linearVelocity;
	float rotationalVelocity;
};

struct DynamicSteeringOutput
{
	ofVec2f linearAcceleration;
	float rotationalAcceleration;
};