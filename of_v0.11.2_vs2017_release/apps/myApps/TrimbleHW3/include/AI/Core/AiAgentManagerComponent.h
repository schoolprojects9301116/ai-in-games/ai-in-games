#pragma once

#include "Engine/Core/Component.h"
#include <vector>

class AiAgentComponent;

class AiAgentManagerComponent : public game_engine::Component
{
public:
	AiAgentManagerComponent(game_engine::GameObject* gameObject);
	virtual ~AiAgentManagerComponent();
	virtual void Update(float dt);

	void AddAiAgent(AiAgentComponent* aiAgent);

private:
	void CheckForStateChange();
	void SetFlockStatus(bool isActive);

	unsigned int ai_state_ = 0;
	std::vector<AiAgentComponent* > ai_agents_;
	bool state_changed_ = true;
	bool use_kinematic_movement_ = true;
	bool use_arrive_ = false;
	bool should_flee_ = false;
	bool should_hide_flock_ = false;
	bool should_face_ = false;
};