#pragma once
#include <vector>
struct DynamicSteeringOutput;
struct RigidBody2D;

namespace MovementAlgorithms
{
namespace Dynamic
{
	DynamicSteeringOutput LookWhereYouAreGoing(const RigidBody2D& character
		, float maxRotationAcceleration
		, float targetTheta
		, float slowTheta
	  , float timeToTarget);

	DynamicSteeringOutput Face(const RigidBody2D& character, RigidBody2D target, float maxRotationAcceleration
		, float targetTheta
		, float slowTheta
	  , float timeToTarget);

	DynamicSteeringOutput Seperation(std::vector<RigidBody2D>& agents, size_t activeAgentIndex, float maxAcceleration
		, float seperationRadius);

	DynamicSteeringOutput Flock(std::vector<RigidBody2D>& agents, size_t leaderIndex, size_t activeAgentIndex
		, float maxSpeed, float maxLinearAcceleration, float timeToTarget);


}
}