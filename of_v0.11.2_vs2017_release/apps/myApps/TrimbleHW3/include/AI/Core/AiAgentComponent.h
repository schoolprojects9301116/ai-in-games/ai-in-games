#pragma once

#include "RigidBody2D.h"
#include "AI/Core/SteeringOutput.h"
#include "Engine/Core/Component.h"
#include "ofColor.h"

#include <vector>

struct BoidTriangle
{
	ofVec2f nosePos;
	ofVec2f bodyPos1;
	ofVec2f bodyPos2;
};

void DrawBoid(game_engine::GameObject* gameObject);

class AiAgentComponent : public game_engine::Component
{
public:

	AiAgentComponent(game_engine::GameObject* gameObject
		, ofVec2f initialPosition
		, float initialOrientation
		, float circleRadius
		, float noseLength
		, float maxSpeed
		, float maxLinearAcceleration
		, float maxRotationSpeed
		, float mass
	  , ofColor color);
	virtual ~AiAgentComponent();
	virtual void Update(float dt);

	void DynamicUpdate(float dt, DynamicSteeringOutput steeringOutput);
	
	ofVec2f Velocity() const;
	void SetVelocity(ofVec2f newVelocity);
	void SetRotation(float newRotation);

	ofVec2f Position() const;
	void Position(ofVec2f newPosition);
	void Orientation(float newOrientation);
	float Orientation() const;
	RigidBody2D RigidBody() const;
	float MaxSpeed();
	void MaxSpeed(float newMaxSpeed);
	float MaxLinearAcceleration();
	void MaxLinearAcceleration(float newMaxAcceleration);
	float GetBoidNoseLength();

	void DrawAiAgent();

	void ClearCrumbs();

private:

	BoidTriangle CalculateBoidTrianglePoints();
	RigidBody2D rb2d_;
	float boid_circle_radius;
	float boid_nose_scale_;
	float max_speed_;
	float max_linear_acceleration_;
	float max_rotation_speed_;
	ofColor color_;

	std::vector<ofVec2f> bread_crumbs_;
	size_t bread_crumb_index_ = 0;
	//BoundingBox2D bounding_box_;
};