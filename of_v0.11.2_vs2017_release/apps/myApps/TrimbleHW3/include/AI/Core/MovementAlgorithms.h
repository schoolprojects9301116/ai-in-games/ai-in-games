#pragma once

struct KinematicSteeringOutput;
struct DynamicSteeringOutput;
struct RigidBody2D;

namespace MovementAlgorithms
{
namespace Kinematic
{
	KinematicSteeringOutput Seek(const RigidBody2D & character
		, const RigidBody2D & target
		, float maxSpeed
	  , bool  shouldFlee);
}
namespace Dynamic
{
	DynamicSteeringOutput Seek(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxLinearAcceleration
	  , bool  shouldFlee);

	DynamicSteeringOutput ArrivePm(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxSpeed
		, float maxAcceleration
		, float slowRadius
		, float targetRadius
	  , float timeToTarget);

	DynamicSteeringOutput Align(const RigidBody2D& character
		, const RigidBody2D& target
		, float maxRotationAcceleration
		, float targetTheta
		, float slowTheta
	  , float timeToTarget);

	DynamicSteeringOutput Wander(const RigidBody2D& character, float offset, float maxRate, float maxLinearAcceleration);

	DynamicSteeringOutput VelocityMatch(const RigidBody2D& character, const RigidBody2D& target,
		float maxLinearAcceleration, float timeToTarget);
}
}
