#pragma once
#include <unordered_map>
#include <stack>
#include "AI/DecisionMaking/AiAction.h"

class GoapState;
class GoapTask;

std::stack<AiAction*> Goap(const GoapState& startState, const GoapState& targetState
	, const std::vector<GoapTask> tasks, std::unordered_map<std::string, AiAction*> actionMap);