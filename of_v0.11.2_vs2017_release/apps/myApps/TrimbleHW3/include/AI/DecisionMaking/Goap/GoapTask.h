#pragma once
#include <string>
#include "AI/DecisionMaking/AiAction.h"

class GoapState;

class GoapTask
{
public:
	GoapTask(std::string taskName, unsigned int preConditions, unsigned int addConditions, unsigned int delConditions,
		bool exactMatchConditions=true);
	~GoapTask();

	std::string Name() const;
	bool MeetsConditions(const GoapState& state) const;
	void ApplyConditions(GoapState& state) const;
	GoapState GetAppliedState(const GoapState& state) const;
	GoapState GetConditionState() const;
	float GetHeuristic(const GoapState& state) const;

private:
	unsigned int pre_conditions_;
	unsigned int add_conditions_;
	unsigned int del_conditions_;
	std::string task_name_;
	bool exact_match_conditions_;
};