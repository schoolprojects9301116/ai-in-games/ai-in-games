#pragma once
#include <string>

class GoapState
{
public:
	GoapState(unsigned int stateInfo = 0, size_t maxBits = sizeof(unsigned int) * 8);
	~GoapState();

	void SetBitState(int idx, bool state);
	void SetState(unsigned int state);
	bool GetBitState(int idx) const;
	unsigned int GetState() const;
	bool operator==(const GoapState& other) const;
	bool operator!=(const GoapState& other) const;
	size_t NumSetBits() const;

private:

	size_t CountSetBits() const;
	unsigned int state_info_;
	size_t max_bits_;
	size_t num_set_bits_;
};