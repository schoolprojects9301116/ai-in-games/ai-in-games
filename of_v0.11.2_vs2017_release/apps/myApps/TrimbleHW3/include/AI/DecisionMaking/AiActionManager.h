#pragma once
#include "AI/DecisionMaking/AiAction.h"
#include "Engine/Core/Component.h"
#include <queue>

	
auto AiActionCompararator = [](const AiAction& actionA, const AiAction& actionB)
{
	return actionA.Priority() < actionB.Priority();
};

class AiActiomCompare
{
public:
	bool operator() (AiAction* actionA, AiAction* actionB)
	{
		return actionA->Priority() < actionB->Priority();
	}
};

class AiActionManager : public game_engine::Component
{
public:
	AiActionManager(game_engine::GameObject* gameObject);
	virtual ~AiActionManager();
	void ScheduleAction(AiAction* action);
	virtual void Update(float dt) override;

private:

	void UpdateActionQueuedTime(float dt);
	void CheckInterrupts();
	void PromoteQueuedActionsToActive();
	void RunActiveActions(float dt);
	std::priority_queue<AiAction*, std::vector<AiAction*>, AiActiomCompare> pending_queue_;
	//std::priority_queue<AiAction*, std::vector<AiAction*>, decltype(AiActionCompararator)> pending_queue_;
	std::vector<AiAction*> active_actions_;
};