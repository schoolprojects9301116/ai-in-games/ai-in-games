#pragma once
#include <string>

enum class AiActionStatus {ACTION_RUNNING, ACTION_SUCCESS, ACTION_FAILED, ACTION_ERROR, ACTION_NONE};

class AiAction
{
public:
	AiAction(int priority, float expiryTime, std::string name);

	int Priority() const;
	bool Expired() const;
	bool IsComplete() const;
	void UpdateQueuedTime(float dt);
	void TerminateAction();
	void CompleteAction();
	void ScheduleAction();
	AiActionStatus Status() const;
	std::string Name() const;

	virtual bool CanInterrupt() const = 0;
	virtual bool CanDoBoth(const AiAction& other) const = 0;
	virtual void Execute(float dt) = 0;


private:
	int priority_;
	float queued_time_;
	float expiry_time_;
	AiActionStatus status_;
	bool is_complete_;
	std::string name_;
};

class AiConditionAction : public AiAction
{
public:
	AiConditionAction(int priority, float expiryTime, std::string actionName);
	virtual ~AiConditionAction();

	bool GetResult();

protected:
	bool result_;
};