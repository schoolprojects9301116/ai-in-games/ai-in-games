#pragma once
#include "AI/DecisionMaking/AiAction.h"

class DecisionTreeNode
{
public:
	DecisionTreeNode();
	virtual ~DecisionTreeNode();
	virtual AiAction& MakeDecision() = 0;
};

class DecisionTreeActionNode : public DecisionTreeNode
{
public:
	DecisionTreeActionNode();
	virtual ~DecisionTreeActionNode();
	virtual AiAction& MakeDecision() override;
	void SetAction(AiAction* action);
private:
	AiAction* action_;
};

class DecisionTreeBranchCondition
{
public:
	DecisionTreeBranchCondition();
	virtual ~DecisionTreeBranchCondition();
	virtual bool CheckCondition() = 0;
};

class DecisionTreeBranchNode : public DecisionTreeNode
{
public:
	DecisionTreeBranchNode();
	virtual ~DecisionTreeBranchNode();
	virtual AiAction& MakeDecision() override;
	void SetTrueNode(DecisionTreeNode* trueNode);
	void SetFalseNode(DecisionTreeNode* falseNode);
	void SetCondition(DecisionTreeBranchCondition* condition);
private:
	DecisionTreeBranchCondition* condition_;
	DecisionTreeNode* true_node_;
	DecisionTreeNode* false_node_;
};