#pragma once
#include "AI/DecisionMaking/AiAction.h"
#include "AI/Core/AiAgentComponent.h"
#include "AI/DecisionMaking/DecisionContainers.h"
#include "Engine/Maps/MapGenerator.h"

class RandomTargetAction : public AiAction
{
public:
	RandomTargetAction(int priority, float expiryTime, float maxX, float maxY, TargetPosition& targetPosition);
	virtual ~RandomTargetAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	float max_x_;
	float max_y_;
	TargetPosition& target_position_;
};

class PathfindAction : public AiAction
{
public:
	PathfindAction(int priority, float expirtyTime, AiAgentComponent& agent, TargetPosition& targetPosition
	, game_engine::maps::LevelMap& map, std::stack<ofVec2f>& pathStack);
	virtual ~PathfindAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;
private:
	AiAgentComponent& agent_;
	TargetPosition& target_position_;
	game_engine::maps::LevelMap& map_;
	std::stack<ofVec2f>& path_stack_;
};

class FollowPathAction : public AiAction
{
public:
	FollowPathAction(int priority, float expiryTime, float pointThreshold, AiAgentComponent& agent, std::stack<ofVec2f>& path);
	virtual ~FollowPathAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	float point_threshold_;
	AiAgentComponent& agent_;
	std::stack<ofVec2f>& path_;
};

class ArriveAction : public AiAction
{
public:
	ArriveAction(int priority, float expiryTime, AiAgentComponent& agent, TargetPosition& targetPosition);
	~ArriveAction();

	virtual bool CanInterrupt() const override;
	virtual bool CanDoBoth(const AiAction& other) const override;
	virtual void Execute(float dt) override;

private:
	AiAgentComponent& agent_;
	TargetPosition& target_position_;
};