#pragma once

#include "AI/DecisionMaking/BehaviorTrees/BehaviorTree.h"

class BehaviorTreeSequenceNode : public BehaviorTreeNode
{
public:
	BehaviorTreeSequenceNode(std::string nodeName);
	virtual ~BehaviorTreeSequenceNode();

	virtual AiAction* Execute(BehaviorTreeTick& tick) override;
};