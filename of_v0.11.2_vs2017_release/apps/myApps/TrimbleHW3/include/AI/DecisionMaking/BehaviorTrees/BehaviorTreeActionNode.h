#pragma once
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTree.h"
#include "AI/DecisionMaking/AiActionManager.h"

class BehaviorTreeActionNode : public BehaviorTreeNode
{
public:
	BehaviorTreeActionNode(std::string nodeName, AiAction* action);
	virtual ~BehaviorTreeActionNode();
	
	virtual AiAction* Execute(BehaviorTreeTick& tick) override;
private:
	AiAction* action_;
	bool action_scheduled_;
};

class BehaviorTreeConditionNode : public BehaviorTreeNode
{
public:
	BehaviorTreeConditionNode(std::string nodeName, AiConditionAction* action);
	virtual ~BehaviorTreeConditionNode();

	virtual AiAction* Execute(BehaviorTreeTick& tick) override;
private:
	AiConditionAction* action_;
	bool action_scheduled_;
};