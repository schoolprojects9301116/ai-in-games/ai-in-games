#pragma once
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTree.h"

class BehaviorTreeInverterNode : public BehaviorTreeNode
{
public:
	BehaviorTreeInverterNode(std::string nodeName);
	~BehaviorTreeInverterNode();

	virtual void Exit() override;
	virtual AiAction* Execute(BehaviorTreeTick& tick) override;
};

class BehaviorTreeUntilSuccessNode : public BehaviorTreeNode
{
public:
	BehaviorTreeUntilSuccessNode(std::string nodeName);
	~BehaviorTreeUntilSuccessNode();

	virtual void Exit() override;
	virtual AiAction* Execute(BehaviorTreeTick& tick) override;
};