#pragma once
#include <vector>
#include "AI/DecisionMaking/AiAction.h"
#include <unordered_map>

struct BehaviorTreeTick
{
	std::unordered_map<std::string, std::string> data;
};

enum class BtNodeStatus{BT_INACTIVE, BT_RUNNING, BT_SUCCESS, BT_FAILED, BT_ERROR};

class BehaviorTreeNode
{
public:
	BehaviorTreeNode(std::string nodeName);
	virtual ~BehaviorTreeNode();

	void AddChild(BehaviorTreeNode* child);
	BtNodeStatus Status() const;

	virtual void Enter();
	virtual void Exit();
	virtual AiAction* Execute(BehaviorTreeTick& tick) = 0;

protected:
	BtNodeStatus status_;
	std::vector<BehaviorTreeNode*> children_;
	std::vector<BehaviorTreeNode*>::iterator active_child_;
	std::string node_name_;
};

class BehaviorTree
{
public:
	BehaviorTree(BehaviorTreeNode* rootNode=nullptr);
	~BehaviorTree();
	AiAction* Execute();
private:
	BehaviorTreeNode* root_node_;
};