#pragma once
#include "AI/DecisionMaking/BehaviorTrees/BehaviorTree.h"

class BehaviorTreeSelectorNode : public BehaviorTreeNode
{
public:
	BehaviorTreeSelectorNode(std::string nodeName);
	virtual ~BehaviorTreeSelectorNode();

	virtual AiAction* Execute(BehaviorTreeTick& tick) override;
};