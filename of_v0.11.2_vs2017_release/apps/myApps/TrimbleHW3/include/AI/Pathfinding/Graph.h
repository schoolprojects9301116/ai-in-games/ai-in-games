#pragma once

#include <array>
#include <vector>
#include <string>

namespace Pathfinding
{
	class Array2D
	{
	public:
		Array2D(size_t x, size_t y);
		~Array2D();

		int& operator()(size_t x, size_t y);
		const int operator()(size_t x, size_t y) const;

		size_t Width();
		size_t Height();

#ifdef _DEBUG
		void PrintArray();
#endif // _DEBUG


	private:
		int** edges_;
		size_t width_;
		size_t height_;
	};

	struct DirectedWeightedEdge
	{
		size_t source;
		size_t sink;
		float priority;
		bool operator==(const DirectedWeightedEdge& other);
		bool operator!=(const DirectedWeightedEdge& other);
	};

	struct AStarStats
	{
		size_t numNodesVisited;
		size_t numNodesConsidered;
		size_t sizeOfMemoryFootPrint;
	};

	std::vector<size_t> AStar(Array2D& graph, size_t startNodeIndex, size_t endNodeIndex, float(*heuristicFunction)(size_t, size_t)
		, AStarStats* outputStats=nullptr);


	Array2D* GenerateGraphFromFile(std::string fileName);

	void CreateRandomLargeGraph(std::string fileName);

	int RunGraphTests();
}