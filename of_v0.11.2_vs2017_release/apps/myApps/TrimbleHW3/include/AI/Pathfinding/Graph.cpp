#include "Graph.h"
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <unordered_set>
#include <unordered_map>

#ifdef _DEBUG
#include <iostream>
#endif // 


namespace Pathfinding
{
	Array2D::Array2D(size_t x, const size_t y) : edges_(new int* [x]), width_(x), height_(y)
	{
		for (size_t i = 0; i < x; i++)
		{
			edges_[i] = new int[y];
			for (size_t j = 0; j < y; j++)
			{
				edges_[i][j] = -1;
			}
		}
	}

	Array2D::~Array2D()
	{
		for (size_t i = 0; i < width_; i++)
		{
			delete[] edges_[i];
		}
		delete[] edges_;
	}

	size_t Array2D::Width()
	{
		return width_;
	}

	size_t Array2D::Height()
	{
		return height_;
	}

	int& Array2D::operator()(size_t x, size_t y)
	{
		return edges_[x][y];
	}

	const int Array2D::operator()(size_t x, size_t y) const
	{
		return edges_[x][y];
	}

	bool DirectedWeightedEdge::operator==(const DirectedWeightedEdge& other)
	{
		return source == other.source && sink == other.sink;
	}

	bool DirectedWeightedEdge::operator!=(const DirectedWeightedEdge& other)
	{
		return !(*this == other);
	}

	std::vector<size_t> AStar(Array2D& graph, size_t startNodeIndex, size_t endNodeIndex, float(*heuristicFunction)(size_t, size_t),
		AStarStats* outputStats)
	{
		if (outputStats != nullptr)
		{
			outputStats->sizeOfMemoryFootPrint = 0;
			outputStats->numNodesVisited = 0;
			outputStats->numNodesConsidered = 0;
		}
		if (startNodeIndex == endNodeIndex)
		{
			std::vector<size_t> path;
			path.push_back(startNodeIndex);
			return path;
		}
		auto compararator = [](const DirectedWeightedEdge& nodeA, const DirectedWeightedEdge& nodeB)
		{
			return nodeA.priority > nodeB.priority;
		};
		std::priority_queue<DirectedWeightedEdge, std::vector<DirectedWeightedEdge>, decltype(compararator)> fringe(compararator);
		std::unordered_map<size_t, std::pair<size_t, float>> visited;

		for (size_t i = 0; i < graph.Height(); i++)
		{
			if (i != startNodeIndex && graph(startNodeIndex, i) >= 0)
			{
				DirectedWeightedEdge startChildNode{ startNodeIndex, i, 0 };
				startChildNode.priority += graph(startNodeIndex, i);
				startChildNode.priority += heuristicFunction(i, endNodeIndex);
				fringe.push(startChildNode);
				visited.insert({ i, {startNodeIndex, startChildNode.priority} });
				if (outputStats != nullptr)
				{
					outputStats->sizeOfMemoryFootPrint += sizeof(DirectedWeightedEdge);
					outputStats->numNodesConsidered++;
				}
			}
		}

		while (!fringe.empty())
		{
			DirectedWeightedEdge currentNode = fringe.top();
			fringe.pop();
			if (outputStats != nullptr)
			{
				outputStats->numNodesVisited++;
			}
			if (currentNode.sink == endNodeIndex)
			{
				break;
			}
			else
			{
				for (size_t i = 0; i < graph.Height(); i++)
				{
					if (graph(currentNode.sink, i) >= 0)
					{
						DirectedWeightedEdge child{ currentNode.sink, i, currentNode.priority};
						child.priority += graph(child.source, child.sink);
						child.priority += heuristicFunction(child.sink, endNodeIndex);
						if (visited.find(child.sink) != visited.end())
						{
							if (visited[child.sink].second <= child.priority)
							{
								continue;
							}
						}
						if (outputStats != nullptr)
						{
							outputStats->sizeOfMemoryFootPrint += sizeof(DirectedWeightedEdge);
							outputStats->numNodesConsidered++;
						}
						fringe.push(child);
						visited[child.sink] = { child.source, child.priority };
					}
				}
			}
		}

		if (outputStats != nullptr)
		{
			outputStats->sizeOfMemoryFootPrint += sizeof(std::priority_queue<DirectedWeightedEdge
				, std::vector<DirectedWeightedEdge>, decltype(compararator)>);
			outputStats->sizeOfMemoryFootPrint += sizeof(std::unordered_map<size_t, std::pair<size_t, float>>);
			outputStats->sizeOfMemoryFootPrint += visited.size() * sizeof(std::pair<size_t, float>);
		}

		std::vector<size_t> path;
		size_t currentNodeIndex = endNodeIndex;
		while (visited.find(currentNodeIndex) != visited.end())
		{
			path.push_back(currentNodeIndex);
			currentNodeIndex = visited[currentNodeIndex].first;
			if (currentNodeIndex == startNodeIndex)
			{
				path.push_back(currentNodeIndex);
				break;
			}
		}
		return path;
	}


	Array2D* GenerateGraphFromFile(std::string fileName)
	{
		std::ifstream inFile;
		Array2D* array2D = nullptr;

		inFile.open(fileName);
		if (inFile.is_open())
		{
			std::string line;
			std::string token;
			std::vector<std::string> splitString;
			bool parseSize = true;
			while (std::getline(inFile, line))
			{
				splitString.clear();
				size_t pos = 0;
				while ((pos = line.find(' ')) != std::string::npos)
				{
					splitString.push_back(line.substr(0, pos));
					line.erase(0, pos + 1);
				}
				splitString.push_back(line);

				if (parseSize)
				{
					if (splitString.size() != 2 || splitString[0] != "p")
					{
						break;
					}
					size_t numNodes = std::stoi(splitString[1]) + 1;
					array2D = new Array2D(numNodes, numNodes);
					parseSize = false;
				}
				else
				{
					if (splitString.size() != 4 || splitString[0] != "e")
					{
						delete array2D;
						break;
					}
					(*array2D)(std::stoi(splitString[1]), std::stoi(splitString[2])) = std::stoi(splitString[3]);
				}

			}
		}
		inFile.close();
		return array2D;
	}

	void CreateRandomLargeGraph(std::string fileName)
	{
		size_t minNumNodes = 2000;
		size_t maxNumNodes = 10000;
		size_t maxWeight = 10;
		size_t numNodes = rand() % maxNumNodes;
		if (numNodes < minNumNodes)
		{
			numNodes += minNumNodes;
		}
		Array2D graph(numNodes, numNodes);
		for (size_t i = 0; i < numNodes; i++)
		{
			size_t numConnections = rand() % 100 + 2;
			size_t connectionsMade = 2;
			std::unordered_set<size_t> seenConnections;
			seenConnections.insert(i);
			if (i == 0)
			{
				graph(i, numNodes - 1) = rand() % maxWeight;
				graph(i, i + 1) = rand() % maxWeight;
				seenConnections.insert(numNodes - 1);
				seenConnections.insert(i + 1);
			}
			else if (i == numNodes - 1)
			{
				graph(i, 0) = rand() % maxWeight;
				graph(i, i - 1) = rand() % maxWeight;
				seenConnections.insert(0);
				seenConnections.insert(i - 1);
			}
			else
			{
				graph(i, i + 1) = rand() % maxWeight;
				graph(i, i - 1) = rand() % maxWeight;
				seenConnections.insert(i + 1);
				seenConnections.insert(i - 1);
			}
			while (connectionsMade < numConnections)
			{
				int weight = rand() % maxWeight + 1;
				size_t connection = rand() % numNodes;
				while (seenConnections.find(connection) != seenConnections.end())
				{
					connection = rand() % numNodes;
				}
				graph(i, connection) = weight;
				seenConnections.insert(connection);
				connectionsMade++;
			}
		}
		std::ofstream outFile;
		outFile.open(fileName);
		if (outFile.is_open())
		{
			std::string writeLine = "p " + std::to_string(numNodes - 1) + "\n";
			outFile << writeLine;
			for (size_t i = 0; i < numNodes; i++)
			{
				for (size_t j = 0; j < numNodes; j++)
				{
					if (graph(i, j) > 0)
					{
						writeLine = "e " + std::to_string(i) + " " + std::to_string(j) + " " + std::to_string(graph(i, j)) + "\n";
						outFile << writeLine;
					}
				}
			}
			outFile.close();
		}
	}

	float ReturnZero(size_t, size_t)
	{
		return 0.0f;
	}

	float DifferenceCalculation(size_t nodeA, size_t nodeB)
	{
		return std::abs((float)nodeB - (float)nodeA);
	}

	float ConstantGuess(size_t nodeA, size_t nodeB)
	{
		return 2.0f;
	}

	int RunIndividualGraphTest(Pathfinding::Array2D* graph, std::string graphName, std::string filePath
		, std::string algorithmName, size_t searchStep, float(*heuristicFunc)(size_t, size_t))
	{
		if (graph == nullptr)
		{
			std::cout << "Invalid graph passed for: " << graphName << std::endl;
			return 1;
		}
		std::ofstream outFile;
		std::string fileName = filePath + graphName + "_" + algorithmName + ".csv";
		outFile.open(fileName);
		if (outFile.is_open())
		{
			std::cout << "Running " << algorithmName << " on: " << graphName << std::endl;
			std::cout << "Reults will be save to: " << fileName << std::endl;

			std::string outputString = graphName;
			std::cout << outputString << std::endl;
			outFile << outputString << std::endl;

			AStarStats aStarStats{ 0, 0, 0 };
			AStarStats totalStats{ 0, 0, 0 };
			size_t numTests = 0;
			for (size_t i = 0; i < graph->Width(); i+= searchStep)
			{
				for (size_t j = 0; j < graph->Height(); j+= searchStep)
				{
					outputString = "Traverse From " + std::to_string(i) + " to " + std::to_string(j);
					std::cout << outputString << std::endl;
					outFile << outputString << std::endl;
					auto path = AStar(*graph, i, j, heuristicFunc, &aStarStats);
					numTests++;
					totalStats.numNodesConsidered += aStarStats.numNodesConsidered;
					totalStats.numNodesVisited += aStarStats.numNodesVisited;
					totalStats.sizeOfMemoryFootPrint += aStarStats.sizeOfMemoryFootPrint;
					outputString.clear();
					for (auto& node : path)
					{
						if (outputString.empty())
						{
							outputString = std::to_string(node);
						}
						else
						{
							outputString = std::to_string(node) + "," + outputString;
						}
					}
					std::cout << "Path" << std::endl;
					outFile << "Path" << std::endl;
					std::cout << outputString << std::endl;
					outFile << outputString << std::endl;
					outputString = "Num Nodes Visited:," + std::to_string(aStarStats.numNodesVisited);
					std::cout << outputString << std::endl;
					outFile << outputString << std::endl;
					outputString = "Num Nodes Considered:," + std::to_string(aStarStats.numNodesConsidered);
					std::cout << outputString << std::endl;
					outFile << outputString << std::endl;
					outputString = "Num Bytes Used:," + std::to_string(aStarStats.sizeOfMemoryFootPrint);
					std::cout << outputString << std::endl;
					outFile << outputString << std::endl;
				}
			}

			outputString = "Averaged Stats:";
			std::cout << outputString << std::endl;
			outFile << outputString << std::endl;
			outputString = "Num Nodes Visited:," + std::to_string(((float)totalStats.numNodesVisited / (float)numTests));
			std::cout << outputString << std::endl;
			outFile << outputString << std::endl;
			outputString = "Num Nodes Considered:," + std::to_string(((float)totalStats.numNodesConsidered / (float)numTests));
			std::cout << outputString << std::endl;
			outFile << outputString << std::endl;
			outputString = "Bytes Used:," + std::to_string(((float)totalStats.sizeOfMemoryFootPrint / (float)numTests));
			std::cout << outputString << std::endl;
			outFile << outputString << std::endl;

			outFile.close();
		}
		else
		{
			std::cout << "Failed to open file: " + filePath << std::endl;
			return 1;
		}
		return 0;
	}

	int RunGraphTests()
	{
		std::cout << "Running Dijstrak and Astar tests" << std::endl;

		Pathfinding::Array2D*  UtahGraph = Pathfinding::GenerateGraphFromFile("GraphData\\UtahMap.txt");
		Pathfinding::Array2D* LargeGraph = Pathfinding::GenerateGraphFromFile("GraphData\\LargeRandomGraph.txt");

		if (RunIndividualGraphTest(UtahGraph, "UtahGraph", "Writeup\\TestResults_", "Dijkstra", 1, &ReturnZero))
		{
			std::cout << "Failed to run Dijkstra for UtahGraph" << std::endl;
			return 1;
		}
		if (RunIndividualGraphTest(UtahGraph, "UtahGraph", "Writeup\\TestResults_", "ConstantGuess", 1, &DifferenceCalculation))
		{
			std::cout << "Failed to Run Constant Guess for UtahGrpah" << std::endl;
			return 1;
		}
		if (RunIndividualGraphTest(UtahGraph, "UtahGraph", "Writeup\\TestResults_", "NodeIdDiff", 1, &ConstantGuess))
		{
			std::cout << "Failed to Run NodeIdDiff for UtahGraph" << std::endl;
			return 1;
		}
		if (RunIndividualGraphTest(LargeGraph, "LargeGraph", "Writeup\\TestResults_", "Dijkstra", 50, &ReturnZero))
		{
			std::cout << "Failed to run Dijkstra for LargeGraph" << std::endl;
			return 1;
		}
		if (RunIndividualGraphTest(LargeGraph, "LargeGraph", "Writeup\\TestResults_", "ConstantGuess", 50, &ConstantGuess))
		{
			std::cout << "Failed to Run Constant Guess for LargeGraph" << std::endl;
			return 1;
		}
		if (RunIndividualGraphTest(LargeGraph, "LargeGraph", "Writeup\\TestResults_", "NodeIdDiff", 50, &DifferenceCalculation))
		{
			std::cout << "Failed to run NodeIdDiff for LargeGraph" << std::endl;
			return 1;
		}

		if (UtahGraph != nullptr)
		{
			delete UtahGraph;
		}
		if (LargeGraph != nullptr)
		{
			delete LargeGraph;
		}
		return 0;
	}


#ifdef _DEBUG
	void Array2D::PrintArray()
	{
		for (size_t y = 0; y < height_; y++)
		{
			std::string row;
			for (size_t x = 0; x < width_; x++)
			{
				row += std::to_string(edges_[x][y]) + " ";
			}
			std::cout << row << std::endl;
		}
	}
#endif // _DEBUG

}